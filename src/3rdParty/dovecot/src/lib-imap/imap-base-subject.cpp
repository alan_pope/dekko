/* Copyright (c) 2002-2015 Dovecot authors, see the included COPYING file */

/* Implementated against draft-ietf-imapext-sort-10 and
   draft-ietf-imapext-thread-12 */

#include "imap-base-subject.h"
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#define FALSE 0
#define TRUE 1


int strncicmp(char const *a, char const *b, int n)
{
    for (;; a++, b++) {
        int d = tolower(*a) - tolower(*b);
        n--;
        if (d != 0 || !*a || !*b || n ==0)
            return d;
    }
}

static void pack_whitespace(char *buf)
{
    char *dest, *data_ptr;
    bool last_lwsp;

    char * const data = (char *) (malloc(strlen(buf) + 1));
    data_ptr = data;
    strcpy(data, buf);

	/* check if we need to do anything */
    while (*data_ptr != '\0') {
        if (*data_ptr == '\t' || *data_ptr == '\n' || *data_ptr == '\r' ||
            (*data_ptr == ' ' && (data_ptr[1] == ' ' || data_ptr[1] == '\t')))
			break;
        data_ptr++;
	}

    if (*data_ptr == '\0') {
        free(data);
        return;
    }

	/* @UNSAFE: convert/pack the whitespace */
    dest = data_ptr; last_lwsp = FALSE;
    while (*data_ptr != '\0') {
        if (*data_ptr == '\t' || *data_ptr == ' ' ||
            *data_ptr == '\r' || *data_ptr == '\n') {
			if (!last_lwsp) {
				*dest++ = ' ';
				last_lwsp = TRUE;
			}
		} else {
            *dest++ = *data_ptr;
			last_lwsp = FALSE;
		}
        data_ptr++;
	}
	*dest = '\0';

    strcpy(buf, dest);
    free(data);
}

static void remove_subj_trailers(char *buf, size_t start_pos,
				 bool *is_reply_or_forward_r)
{
	const char *data;
	size_t orig_size, size;

    orig_size = strlen(buf) + 1;
    /* subj-trailer    = "(fwd)" / WSP */
    data = buf;

	if (orig_size < 1) /* size includes trailing \0 */
		return;

	for (size = orig_size-1; size > start_pos; ) {
		if (data[size-1] == ' ')
			size--;
		else if (size >= 5 &&
			 memcmp(data + size - 5, "(FWD)", 5) == 0) {
			*is_reply_or_forward_r = TRUE;
			size -= 5;
		} else {
			break;
		}
	}

	if (size != orig_size-1) {
        buf[size] = '\0';
	}
}

static bool remove_blob(const char **datap)
{
	const char *data = *datap;

	if (*data != '[')
		return FALSE;

	data++;
	while (*data != '\0' && *data != '[' && *data != ']')
		data++;

	if (*data != ']')
		return FALSE;

	data++;
	if (*data == ' ')
		data++;

	*datap = data;
	return TRUE;
}

static bool remove_subj_leader(char *buf, size_t *start_pos,
			       bool *is_reply_or_forward_r)
{
	const char *data, *orig_data;
	bool ret = FALSE;

	/* subj-leader     = (*subj-blob subj-refwd) / WSP

	   subj-blob       = "[" *BLOBCHAR "]" *WSP
	   subj-refwd      = ("re" / ("fw" ["d"])) *WSP [subj-blob] ":"

	   BLOBCHAR        = %x01-5a / %x5c / %x5e-7f
	                   ; any CHAR except '[' and ']' */
    orig_data = buf;
	orig_data += *start_pos;
	data = orig_data;

	if (*data == ' ') {
		/* independent from checks below - always removed */
		data++; orig_data++;
		*start_pos += 1;
		ret = TRUE;
	}

	while (*data == '[') {
		if (!remove_blob(&data))
			return ret;
	}

    if (strncicmp(data, "RE", 2) == 0)
		data += 2;
    else if (strncicmp(data, "FWD", 3) == 0)
		data += 3;
    else if (strncicmp(data, "FW", 2) == 0)
		data += 2;
	else
		return ret;

	if (*data == ' ')
		data++;

	if (*data == '[' && !remove_blob(&data))
		return ret;

	if (*data != ':')
		return ret;

	data++;
	*start_pos += (size_t)(data - orig_data);
	*is_reply_or_forward_r = TRUE;
	return TRUE;
}

static bool remove_blob_when_nonempty(char *buf, size_t *start_pos)
{
	const char *data, *orig_data;

    orig_data = buf;
	orig_data += *start_pos;
	data = orig_data;
	if (*data == '[' && remove_blob(&data) && *data != '\0') {
		*start_pos += (size_t)(data - orig_data);
		return TRUE;
	}

	return FALSE;
}

static bool remove_subj_fwd_hdr(char *buf, size_t *start_pos,
				bool *is_reply_or_forward_r)
{
    char *data;
	size_t size;

	/* subj-fwd        = subj-fwd-hdr subject subj-fwd-trl
	   subj-fwd-hdr    = "[fwd:"
	   subj-fwd-trl    = "]" */
    size = strlen(buf) + 1;
    data = buf;

	if (strncmp(data + *start_pos, "[FWD:", 5) != 0)
		return FALSE;

	if (data[size-2] != ']')
		return FALSE;

	*is_reply_or_forward_r = TRUE;

    data[size-2] = '\0';

	*start_pos += 5;
	return TRUE;
}

char * const imap_get_base_subject(const char *subject,
                    bool *is_reply_or_forward_r)
{
    char *buf;
	size_t start_pos, subject_len;
	bool found;

	*is_reply_or_forward_r = FALSE;

	subject_len = strlen(subject);
    buf = (char *) (malloc(subject_len + 1));
    strcpy(buf, subject);

	/* (1) Convert any RFC 2047 encoded-words in the subject to
	   UTF-8.  Convert all tabs and continuations to space.
	   Convert all multiple spaces to a single space. */
    //assert subject comes in as utf8 already
//	message_header_decode_utf8((const unsigned char *)subject, subject_len,
//				   buf, uni_utf8_to_decomposed_titlecase);

	pack_whitespace(buf);

	start_pos = 0;
	do {
		/* (2) Remove all trailing text of the subject that matches
		   the subj-trailer ABNF, repeat until no more matches are
		   possible. */
		remove_subj_trailers(buf, start_pos, is_reply_or_forward_r);

		do {
			/* (3) Remove all prefix text of the subject that
			   matches the subj-leader ABNF. */
			found = remove_subj_leader(buf, &start_pos,
						   is_reply_or_forward_r);

			/* (4) If there is prefix text of the subject that
			   matches the subj-blob ABNF, and removing that prefix
			   leaves a non-empty subj-base, then remove the prefix
			   text. */
			found = remove_blob_when_nonempty(buf, &start_pos) ||
				found;

			/* (5) Repeat (3) and (4) until no matches remain. */
		} while (found);

		/* (6) If the resulting text begins with the subj-fwd-hdr ABNF
		   and ends with the subj-fwd-trl ABNF, remove the
		   subj-fwd-hdr and subj-fwd-trl and repeat from step (2). */
	} while (remove_subj_fwd_hdr(buf, &start_pos, is_reply_or_forward_r));

	/* (7) The resulting text is the "base subject" used in the
	   SORT. */

    strcpy(buf, buf + start_pos);

    return buf;
}
