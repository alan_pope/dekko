#ifndef TESTBASESUBJECT_H
#define TESTBASESUBJECT_H

#include <QObject>
class TestBaseSubject: public QObject
{
    Q_OBJECT
public:
    void after();

private slots:
    void testGetBaseSubject();
    void testGetBaseSubject2();

private:
    QString target;
    QString result;
};

#endif // TESTBASESUBJECT_H
