#include "TestBaseSubject.h"
#include "../src/lib-imap/imap-base-subject.h"
#include <QTest>
#include <QString>
#include <QDebug>

void TestBaseSubject::after()
{
    bool is_reply;
    char* base = imap_get_base_subject(target.toLocal8Bit().data(), &is_reply);
    QString baseS = QString::fromLocal8Bit(base);
    QVERIFY(baseS == result);
    delete base;
}

void TestBaseSubject::testGetBaseSubject()
{
    target = "[Bug 1479779] Re: BQ (r24): sending mails with Dekko is terrible slow";
    result = "BQ (r24): sending mails with Dekko is terrible slow";
    after();
}

void TestBaseSubject::testGetBaseSubject2()
{
    target = "Fwd: [ABC Support] Re: X";
    result = "X";
    after();
}

QTEST_MAIN(TestBaseSubject)
