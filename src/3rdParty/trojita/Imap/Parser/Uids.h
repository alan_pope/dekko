#ifndef UIDS_H
#define UIDS_H
#include <QVector>

namespace Imap {

typedef QVector<uint> Uids;

}

#endif // UIDS_H
