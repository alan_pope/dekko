/* Copyright (C) 2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko mail client

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3 or any later version
   accepted by the membership of KDE e.V. (or its successor approved
   by the membership of KDE e.V.), which shall act as a proxy
   defined in Section 14 of version 3 of the license.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LOCALORDEREDSUBJECTTHREADTASK_H
#define LOCALORDEREDSUBJECTTHREADTASK_H

#include <QObject>
#include <QModelIndex>
#include <QSet>
#include <QDateTime>
#include "../Parser/ThreadingNode.h"
#include "ImapTask.h"

namespace Imap {
namespace Mailbox {
class Model;
class TestLocalOrderedSubjectThreadTask;

struct UidSubject {
    int uid; // -1 for unfetched
    QString baseSubject;
    QDateTime time;
    UidSubject() { }
    UidSubject(int uid, QString baseSubject = "", QDateTime time = QDateTime())
        : uid(uid), baseSubject(baseSubject), time(time) { }
};

//TODO incremental
/**
 * A task that is run locally, to simulate mail server thread ability. Use ORDEREDSUBJECT as the algorithm.
 */
class LocalOrderedSubjectThreadTask : public ImapTask
{
    Q_OBJECT
public:
    LocalOrderedSubjectThreadTask(Imap::Mailbox::Model *model, QModelIndex mailboxIndex);

    virtual void perform();
    virtual bool needsMailbox() const { return true; }
    virtual QVariant taskData(const int role) const { return QVariant(); }

signals:
    void threadingAvailable(const QModelIndex &mailbox, const QByteArray &algorithm,
                                 const QStringList &searchCriteria, const QVector<Imap::Responses::ThreadingNode> &mapping);

public slots:
    /**
     * After a mail's subject has been fetched, remove it from the unfetched list.
     * If unfetched list is empty, retry workflow() which will run thread algorithm next.
     */
    void handleDataChanged(QModelIndex topLeft, QModelIndex bottomRight);

private slots:
    /**
     * Entry of thread workflow. First we should have all mails' subjects. If we don't, 
     * we fetch them and wait for all mails to be fetched.
     * Then we run ORDEREDSUBJECT algorithm on all mails.
     */
    void workflow();

private:
    /**
     * run ORDEREDSUBJECT threading algorithm on all mails in the mailbox. 
     * Assume all mails are fetched already.
     */
    void runOrderedSubject();
    QVector<Imap::Responses::ThreadingNode> runOrderedSubjectImp(QList<UidSubject> list);
    /**
     * Trigger fetch of all mails in the mailbox.
     * Once a mail is fetched handleDataChanged() will be triggered and decide to restart workflow when all mails are fetched.
     */
    void triggerFetch();
    /**
     * check if all mails are fetched. A prerequisite of runOrderedSubject()
     */
    bool hasFullData();

    QModelIndex m_mailboxIndex;
    QSet<int> m_unfetchedSet;
    bool m_fetched;
    bool m_waitingForFetch;

    friend class Imap::Mailbox::TestLocalOrderedSubjectThreadTask;
};

}
}

#endif // LOCALORDEREDSUBJECTTHREADTASK_H
