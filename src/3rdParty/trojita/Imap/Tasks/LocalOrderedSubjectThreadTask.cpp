/* Copyright (C) 2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of the Dekko email client for Ubuntu Touch/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "LocalOrderedSubjectThreadTask.h"

#include <QModelIndex>
#include "../Model/Model.h"
#include "../Model/ItemRoles.h"
#include "../dovecot/src/lib-imap/imap-base-subject.h"
#include "KeepMailboxOpenTask.h"
#include <cassert>

namespace Imap {
namespace Mailbox {

LocalOrderedSubjectThreadTask::LocalOrderedSubjectThreadTask(Model *model, QModelIndex mailboxIndex)
    : ImapTask(model), m_mailboxIndex(mailboxIndex), m_waitingForFetch(false)
{
    //model is 0 in unit tests
    if (model) {
        model->findTaskResponsibleFor(mailboxIndex)->addDependentTask(this);
        connect(this, SIGNAL(threadingAvailable(QModelIndex, QByteArray, QStringList, QVector<Imap::Responses::ThreadingNode>)),
            model, SIGNAL(threadingAvailable(QModelIndex, QByteArray, QStringList, QVector<Imap::Responses::ThreadingNode>)));
    }
}

bool LocalOrderedSubjectThreadTask::hasFullData()
{
    int mailCount = m_mailboxIndex.data(Imap::Mailbox::RoleTotalMessageCount).toInt();
    QList<UidSubject> list;
    for (int i = 0; i < mailCount; i++) {
        QModelIndex mailIndex = m_mailboxIndex.child(0, 0).child(mailCount - i - 1, 0);
        bool isFetched = mailIndex.data(Imap::Mailbox::RoleIsFetched).toBool();
        if (!isFetched) {
            return false;
        }
    }
    return true;
}

void LocalOrderedSubjectThreadTask::perform()
{
    workflow();
}

void LocalOrderedSubjectThreadTask::workflow() {
    if (hasFullData()) {
        runOrderedSubject();
    } else {
        triggerFetch();
    }
}

void LocalOrderedSubjectThreadTask::handleDataChanged(QModelIndex topLeft, QModelIndex bottomRight)
{
    if (topLeft.parent().parent() == m_mailboxIndex) {
        assert(topLeft.row() == bottomRight.row());
        if (topLeft.data(Imap::Mailbox::RoleIsFetched).toBool()) {
            int uid = topLeft.data(Imap::Mailbox::RoleMessageUid).toInt();
            if (m_unfetchedSet.contains(uid)) {
                m_unfetchedSet.remove(uid);
            }
            if (m_unfetchedSet.isEmpty() && m_waitingForFetch) {
                m_waitingForFetch = false;
                QTimer::singleShot(100, this, SLOT(workflow()));
                disconnect(model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(handleDataChanged(QModelIndex, QModelIndex)));
            }
        }
    }
}

bool uidSubjectLessThan(const UidSubject &v1, const UidSubject &v2)
{
    if (v1.baseSubject < v2.baseSubject) {
        return true;
    } else if (v1.baseSubject > v2.baseSubject) {
        return false;
    } else {
        return v1.time > v2.time;
    }
}

typedef struct {
    int uid;
    QDateTime time;
    QList<int> childs;
} ThreadHead;

bool threadHeadLessThan(const ThreadHead &v1, const ThreadHead &v2) {
    return v1.time < v2.time;
}

QVector<Imap::Responses::ThreadingNode> LocalOrderedSubjectThreadTask::runOrderedSubjectImp(QList<UidSubject> list)
{
    if (list.length() == 0)
        return QVector<Imap::Responses::ThreadingNode>();
    qSort(list.begin(), list.end(), uidSubjectLessThan);

    //collapse on baseSubject
    QList<ThreadHead> threadHeadList;
    ThreadHead first;
    first.uid = list[0].uid;
    first.time = list[0].time;
    threadHeadList.append(first);
    QString lastSubject = list[0].baseSubject;
    for (int i = 1; i < list.length(); i++) {
        if (list[i].baseSubject == lastSubject) {
            threadHeadList.last().childs.append(list[i].uid);
        } else {
            ThreadHead last;
            last.uid = list[i].uid;
            last.time = list[i].time;
            threadHeadList.append(last);
            lastSubject = list[i].baseSubject;
        }
    }
    qSort(threadHeadList.begin(), threadHeadList.end(), threadHeadLessThan);

    // transform internal data structure: ThreadHead to external structure Imap::Responses::ThreadingNode
    QVector<Imap::Responses::ThreadingNode> threadNodeList;
    for (int i = 0; i < threadHeadList.length(); i++) {
        ThreadHead th = threadHeadList[i];
        QVector<Imap::Responses::ThreadingNode> children;
        for (int j = 0; j < th.childs.length(); j++) {
            children.append(Imap::Responses::ThreadingNode(th.childs[j]));
        }
        Imap::Responses::ThreadingNode tn(th.uid, children);
        threadNodeList.append(tn);
    }

    return threadNodeList;
}

void LocalOrderedSubjectThreadTask::triggerFetch()
{
    int mailCount = m_mailboxIndex.data(Imap::Mailbox::RoleTotalMessageCount).toInt();
    connect(model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(handleDataChanged(QModelIndex, QModelIndex)), Qt::QueuedConnection);
    m_unfetchedSet.clear();
    m_waitingForFetch = true;
    for (int i = 0; i < mailCount; i++) {
        QModelIndex mailIndex = m_mailboxIndex.child(0, 0).child(mailCount - i - 1, 0);
        bool isFetched = mailIndex.data(Imap::Mailbox::RoleIsFetched).toBool();
        if (!isFetched) {
            m_unfetchedSet.insert(mailIndex.data(Imap::Mailbox::RoleMessageUid).toInt());
            mailIndex.data(Imap::Mailbox::RoleMessageSubject);
        }
    }
}

void LocalOrderedSubjectThreadTask::runOrderedSubject()
{
    QList<UidSubject> list;
    int mailCount = m_mailboxIndex.data(Imap::Mailbox::RoleTotalMessageCount).toInt();
    for (int i = 0; i < mailCount; i++) {
        QModelIndex mailIndex = m_mailboxIndex.child(0, 0).child(mailCount - i - 1, 0);
        UidSubject s;
        s.uid = mailIndex.data(Imap::Mailbox::RoleMessageUid).toInt();
        bool tmp;
        s.baseSubject = QString::fromUtf8(
                    QByteArray(
                        imap_get_base_subject(
                            mailIndex.data(Imap::Mailbox::RoleMessageSubject).toString().toUtf8().data(), &tmp)
                        )
                    );
        s.time = mailIndex.data(Imap::Mailbox::RoleMessageDate).toDateTime();
        list.append(s);
    }

    QVector<Imap::Responses::ThreadingNode> threadNodeList = runOrderedSubjectImp(list);

    emit threadingAvailable(m_mailboxIndex, QString("ORDEREDSUBJECT").toLocal8Bit(), QStringList() << QLatin1String("ALL"), threadNodeList);
    if (model)
        _completed();
}

}
}
