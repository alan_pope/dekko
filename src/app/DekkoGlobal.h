#ifndef DEKKOGLOBAL_H
#define DEKKOGLOBAL_H
#include <QtGlobal>
#include <QObject>

class NewAccountType : public QObject {
    Q_OBJECT
    Q_ENUMS(Type)
public:
    inline NewAccountType(QObject *parent = 0) : QObject(parent)  {}
    enum Type { IMAP, SMTP, PRESET, IMAP_WITH_ADDED_SMTP, ONLINE_ACCOUNT };
};

class DekkoGlobal
{
public:
    static void registerDekkoTypes(const char* uri, int major = 0, int minor = 2);

    static void registerTrojitaCoreTypes(const char *uri, int major = 0, int minor = 1);
};

#endif // DEKKOGLOBAL_H
