#include "CustomQQuickViewFactory.h"

namespace Dekko {
namespace Utils {

static QWeakPointer<QQuickView> view;
QSharedPointer<QQuickView> CustomQQuickViewFactory::instance()
{
    QSharedPointer<QQuickView> quickView = view.toStrongRef();
    if (quickView.isNull()) {
            quickView = QSharedPointer<QQuickView>(new QQuickView);
            view = quickView;
    }
    return view;
}

}
}
