/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of the Dekko

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3 or any later version
   accepted by the membership of KDE e.V. (or its successor approved
   by the membership of KDE e.V.), which shall act as a proxy
   defined in Section 14 of version 3 of the license.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ACCOUNTSLISTMODEL_H
#define ACCOUNTSLISTMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QSortFilterProxyModel>
#include "app/Accounts/Account.h"

class QSettings;

namespace Dekko
{
namespace Models
{
/*! This model displays a lightweight list of accounts using only the AccountProfile from each account
 *
 * It has no functionlaity bar getting useful info i.e an accountId for a specific account
 * This makes it faster to load an accounts list than having to reload all settings for all accounts.
 *
*/
class AccountsListModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(QAbstractItemModel *srcModel READ srcModel WRITE setSrcModel NOTIFY srcModelChanged)
    Q_ENUMS(FilterRole)
public:
    explicit AccountsListModel(QObject *parent = 0);

    enum FilterRole {
        FILTER_IMAP = Qt::UserRole + 1,
        FILTER_SMTP,
        FILTER_IMAP_SMTP,
        FILTER_INVALID,
    };

    QAbstractItemModel *srcModel() const;

    void setSrcModel(QAbstractItemModel * arg);

    int count() const;

signals:
    void countChanged();
    void srcModelChanged();

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;

private:
};

}
}

#endif // ACCOUNTSLISTMODEL_H
