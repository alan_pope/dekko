/* Copyright (C) 2014-2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MESSAGEMODEL_H
#define MESSAGEMODEL_H

#include <QDateTime>
#include <QDebug>
#include <QPersistentModelIndex>
#include <QVariant>
#include "Imap/Model/OneMessageModel.h"
#include "Imap/Model/SubtreeModel.h"
#include "Imap/Model/Model.h"

namespace Dekko
{
namespace Models
{

class MessageModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QObject * imapModel READ imapModel WRITE setImapModel)
    // Message envelope properties
    Q_PROPERTY(QString subject READ subject NOTIFY envelopeChanged)
    Q_PROPERTY(QDateTime date READ date NOTIFY envelopeChanged)
    Q_PROPERTY(QVariantList from READ from NOTIFY envelopeChanged)
    Q_PROPERTY(QVariantList to READ to NOTIFY envelopeChanged)
    Q_PROPERTY(QVariantList cc READ cc NOTIFY envelopeChanged)
//    Q_PROPERTY(QVariantList replyTo READ replyTo NOTIFY envelopeChanged)
//    Q_PROPERTY(QByteArray inReplyTo READ inReplyTo NOTIFY envelopeChanged)
    Q_PROPERTY(QByteArray messageId READ messageId NOTIFY envelopeChanged)
    Q_PROPERTY(bool hasValidIndex READ hasValidIndex NOTIFY messageChanged)
    Q_PROPERTY(QModelIndex messageIndex READ messageIndex NOTIFY envelopeChanged)
    Q_PROPERTY(bool isListPost READ isListPost NOTIFY envelopeChanged)
    Q_PROPERTY(QList<QByteArray> references READ references NOTIFY envelopeChanged)
    // Message part properties
    Q_PROPERTY(QModelIndex messageFirstChildIndex READ messageFirstChildIndex NOTIFY envelopeChanged)
    Q_PROPERTY(int uid READ uid NOTIFY envelopeChanged)
    Q_PROPERTY(QString mailbox READ mailbox WRITE setMailbox NOTIFY mailboxChanged)
    Q_PROPERTY(QModelIndex mainTextPartIndex READ mainTextPartIndex NOTIFY mainTextPartIndexChanged)
    // Message flags
    Q_PROPERTY(bool isMarkedDeleted READ isMarkedDeleted NOTIFY flagsChanged)
    Q_PROPERTY(bool isMarkedRead READ isMarkedRead  NOTIFY flagsChanged)
    Q_PROPERTY(bool isMarkedForwarded READ isMarkedForwarded NOTIFY flagsChanged)
    Q_PROPERTY(bool isMarkedReplied READ isMarkedReplied NOTIFY flagsChanged)
    Q_PROPERTY(bool isMarkedRecent READ isMarkedRecent NOTIFY flagsChanged)
    Q_PROPERTY(bool isMarkedFlagged READ isMarkedFlagged NOTIFY flagsChanged)
public:
    explicit MessageModel(QObject * parent = 0);
    MessageModel(QObject *parent, Imap::Mailbox::Model *model);

    void initialize();

    QString subject() const;
    QDateTime date() const;
    QVariantList from() const;
    QVariantList to() const;
    QVariantList cc() const;

    bool hasValidIndex() const;
    bool isListPost() const;
    QList<QByteArray> references() const;
    QByteArray messageId() const;
    QModelIndex messageFirstChildIndex() const;
    QModelIndex messageIndex() const;

    QObject * imapModel();
    void setImapModel(QObject * imapModel);
    void setModel(Imap::Mailbox::Model *model);
    uint uid();
    QString mailbox();
    void setMailbox(QString mailbox);
    QModelIndex mainTextPartIndex();

    bool isMarkedDeleted() const;
    bool isMarkedRead() const;
    bool isMarkedForwarded() const;
    bool isMarkedReplied() const;
    bool isMarkedRecent() const;
    bool isMarkedFlagged() const;

    Q_INVOKABLE void openMessage();
    Q_INVOKABLE void markMessageDeleted();
    Q_INVOKABLE void markMessageRead(bool marked);
    Q_INVOKABLE void markMessageFlagged();
    Q_INVOKABLE void setMessageModelIndex(QModelIndex index);
    Q_INVOKABLE QModelIndex getModelIndexFromPartId(QString partId);
    Q_INVOKABLE QList<QByteArray> replyReferences() const;

signals:
    void mailboxChanged();
    void mainTextPartIndexChanged();
    void messageChanged();
    void envelopeChanged();
    void flagsChanged();

public slots:
    void setMessage(const QString &mboxName, const uint uid);
    void setMessage(const QModelIndex &message);
    void messageClosed();

private slots:
    void handleModelDataChanged(const QModelIndex &topLeft, const QModelIndex &btmRight);
private:
    Imap::Mailbox::Model *m_imapModel;
    int m_uid;
    QString m_mailbox;
    QPersistentModelIndex m_message;
    QModelIndex m_mainPartIndex;
    QPointer<Imap::Mailbox::SubtreeModelOfModel> m_subtree;

};

}
}

#endif
