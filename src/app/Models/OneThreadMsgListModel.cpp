/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>
 * Copyright (C) 2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "OneThreadMsgListModel.h"
#include "3rdParty/trojita/Imap/Model/ThreadingMsgListModel.h"
#include <QDebug>
namespace Dekko
{
namespace Models
{
OneThreadMsgListModel::OneThreadMsgListModel(QObject *parent) :
    QAbstractProxyModel(parent), m_threadRootOffset(-1)
{
}

QAbstractItemModel *OneThreadMsgListModel::threadingModel()
{
    return sourceModel();
}

QModelIndex OneThreadMsgListModel::threadRootItem()
{
    return m_threadRoot;
}

int OneThreadMsgListModel::threadRootItemOffset() const
{
    return m_threadRootOffset;
}

QHash<int, QByteArray> OneThreadMsgListModel::roleNames() const
{
    if (sourceModel()) {
        return sourceModel()->roleNames();
    } else {
        return QHash<int, QByteArray>();
    }
}

QModelIndex OneThreadMsgListModel::index(int row, int column, const QModelIndex &parent) const
{
    return createIndex(row, column, (void *) 0);
}

QModelIndex OneThreadMsgListModel::parent(const QModelIndex &child) const
{
    return QModelIndex();
}

int OneThreadMsgListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return sourceModel()->rowCount(m_threadRoot) + 1;
}

int OneThreadMsgListModel::columnCount(const QModelIndex &parent) const
{
    return 1;
}

QModelIndex OneThreadMsgListModel::mapToSource(const QModelIndex &proxyIndex) const
{
    if (proxyIndex.row() == 0) {
        return m_threadRoot;
    } else {
        return sourceModel()->index(proxyIndex.row() - 1, 0, m_threadRoot);
    }
}

QModelIndex OneThreadMsgListModel::mapFromSource(const QModelIndex &sourceIndex) const
{
    if (!sourceIndex.parent().isValid()) {
        return index(0, 0);
    } else {
        return index(sourceIndex.row() + 1, 0);
    }
}

void OneThreadMsgListModel::setThreadingModel(QAbstractItemModel *threadingModel)
{
    Imap::Mailbox::ThreadingMsgListModel *model = qobject_cast<Imap::Mailbox::ThreadingMsgListModel *>(threadingModel);
    Q_ASSERT(model);
    if (sourceModel() != NULL) {
        disconnect(sourceModel(), 0, this, 0);
    }
    if (model != sourceModel()) {
        setSourceModel(model);
        connect(model, SIGNAL(modelAboutToBeReset()), this, SLOT(handleModelAboutToBeReset()));
        connect(model, SIGNAL(modelReset()), this, SLOT(handleModelReset()));
        connect(model, SIGNAL(layoutAboutToBeChanged()), this, SIGNAL(layoutAboutToBeChanged()));
        connect(model, SIGNAL(layoutChanged()), this, SIGNAL(layoutChanged()));
        connect(model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(handleDataChanged(QModelIndex,QModelIndex)));
        connect(model, SIGNAL(rowsAboutToBeRemoved(QModelIndex,int,int)), this, SLOT(handleRowsAboutToBeRemoved(QModelIndex,int,int)));
        connect(model, SIGNAL(rowsRemoved(QModelIndex,int,int)), this, SLOT(handleRowsRemoved(QModelIndex,int,int)));
        connect(model, SIGNAL(rowsAboutToBeInserted(QModelIndex,int,int)), this, SLOT(handleRowsAboutToBeInserted(QModelIndex,int,int)));
        connect(model, SIGNAL(rowsInserted(QModelIndex,int,int)), this, SLOT(handleRowsInserted(QModelIndex,int,int)));
        emit modelsChanged();
    }
}

void OneThreadMsgListModel::setThreadRootItem(QModelIndex rootIndex)
{
    m_threadRoot = rootIndex;
    emit threadRootChanged();
}

void OneThreadMsgListModel::setThreadRootItemByOffset(const int row)
{
    if (m_threadRootOffset == row) {
        return;
    }
    m_threadRootOffset = row;
    setThreadRootItem(mapToSource(index(row, 0)));
}

void OneThreadMsgListModel::setOriginalRoot()
{
    setThreadRootItem(QModelIndex());
}

void OneThreadMsgListModel::handleDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    QModelIndex first = mapFromSource(topLeft);
    QModelIndex last = mapFromSource(bottomRight);

    if (!first.isValid() || !last.isValid()) {
        if (!m_threadRoot.isValid()) {
            emit dataChanged(QModelIndex(), QModelIndex());
        } else {
            // No idea how we can even get here??
        }
        return;
    }
    if (first.parent() == last.parent() ) {
        emit dataChanged(first, last);
    } else {
        // ignore this as it's either a batched update or
        // an index in the msglist but not in out thread tree
    }
}

void OneThreadMsgListModel::handleModelAboutToBeReset()
{
    beginResetModel();
}

void OneThreadMsgListModel::handleModelReset()
{
    endResetModel();
}

void OneThreadMsgListModel::handleRowsAboutToBeRemoved(const QModelIndex &parent, int first, int last)
{
    beginRemoveRows(mapFromSource(parent), first, last);
}

void OneThreadMsgListModel::handleRowsRemoved(const QModelIndex &parent, int first, int last)
{
    Q_UNUSED(first);
    Q_UNUSED(last);
    Q_UNUSED(parent);

    if (!m_threadRoot.isValid()) {
        // Well our thread root disapeared, let's reset the model
        // and just return now
        beginResetModel();
        endResetModel();
        return;
    }
    endRemoveRows();
}

void OneThreadMsgListModel::handleRowsAboutToBeInserted(const QModelIndex &parent, int first, int last)
{
    beginInsertRows(mapFromSource(parent), first, last);
}

void OneThreadMsgListModel::handleRowsInserted(const QModelIndex &parent, int first, int last)
{
    Q_UNUSED(first);
    Q_UNUSED(last);
    Q_UNUSED(parent);
    endInsertRows();
}
}
}
