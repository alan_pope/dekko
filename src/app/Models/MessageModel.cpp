/* Copyright (C) 2014-2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "MessageModel.h"
#include <QUrl>
#include "app/Utils/FindTextPart.h"

namespace Dekko
{
namespace Models
{

MessageModel::MessageModel(QObject *parent)
    : QObject(parent), m_imapModel(0)
{
}

MessageModel::MessageModel(QObject *parent, Imap::Mailbox::Model *model) :
    QObject(parent), m_imapModel(model)
{
    initialize();
}

void MessageModel::initialize()
{
    m_subtree = new Imap::Mailbox::SubtreeModelOfModel(this);
    m_subtree->setSourceModel(m_imapModel);
    connect(m_subtree, SIGNAL(modelReset()), this, SIGNAL(envelopeChanged()));
    connect(m_subtree, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SIGNAL(envelopeChanged()));
    connect(this, SIGNAL(envelopeChanged()), this, SIGNAL(flagsChanged()));
    connect(m_imapModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(handleModelDataChanged(QModelIndex,QModelIndex)));
}

QString MessageModel::subject() const
{
    return m_message.data(Imap::Mailbox::RoleMessageSubject).toString();
}

QDateTime MessageModel::date() const
{
    return m_message.data(Imap::Mailbox::RoleMessageDate).toDateTime();
}

QVariantList MessageModel::from() const
{
    return m_message.data(Imap::Mailbox::RoleMessageFrom).toList();
}

QVariantList MessageModel::to() const
{
    return m_message.data(Imap::Mailbox::RoleMessageTo).toList();
}

QVariantList MessageModel::cc() const
{
    return m_message.data(Imap::Mailbox::RoleMessageCc).toList();
}

bool MessageModel::hasValidIndex() const
{
    return m_message.isValid();
}

bool MessageModel::isListPost() const
{
    QList<QUrl> listPost;
    Q_FOREACH(const QVariant &item, m_message.data(Imap::Mailbox::RoleMessageHeaderListPost).toList()) {
        listPost << item.toUrl();
    }
    return !listPost.isEmpty();
}

QList<QByteArray> MessageModel::references() const
{
    return m_message.data(Imap::Mailbox::RoleMessageHeaderReferences).value<QList<QByteArray> >();
}

QByteArray MessageModel::messageId() const
{
    return m_message.data(Imap::Mailbox::RoleMessageMessageId).toByteArray();
}

QModelIndex MessageModel::messageFirstChildIndex() const
{
    return m_message.child(0, 0);
}

QModelIndex MessageModel::messageIndex() const
{
    return m_message;
}

void MessageModel::openMessage()
{
    setMessage(m_mailbox, m_uid);
}

void MessageModel::markMessageDeleted()
{
    if (!m_message.isValid()) {
        return;
    }
    Imap::Mailbox::Model *model = const_cast<Imap::Mailbox::Model *>(dynamic_cast<const Imap::Mailbox::Model *>(m_message.model()));
    Q_ASSERT(model);
    if (!model->isNetworkAvailable()) {
        return;
    }
    m_imapModel->markMessagesDeleted(QModelIndexList() << m_message, isMarkedDeleted() ? Imap::Mailbox::FLAG_REMOVE : Imap::Mailbox::FLAG_ADD);
}

void MessageModel::markMessageRead(bool marked)
{
    if (!m_message.isValid())
        return;
    Imap::Mailbox::Model *model = const_cast<Imap::Mailbox::Model *>(dynamic_cast<const Imap::Mailbox::Model *>(m_message.model()));
    Q_ASSERT(model);
    if (!model->isNetworkAvailable())
        return;
    if (!m_message.data(Imap::Mailbox::RoleMessageIsMarkedRead).toBool())
        model->markMessagesRead(QModelIndexList() << m_message, Imap::Mailbox::FLAG_ADD);
}

void MessageModel::markMessageFlagged()
{
    if (!m_message.isValid()) {
        return;
    }
    Imap::Mailbox::Model *model = const_cast<Imap::Mailbox::Model *>(dynamic_cast<const Imap::Mailbox::Model *>(m_message.model()));
    Q_ASSERT(model);
    if (!model->isNetworkAvailable()) {
        return;
    }
    model->setMessageFlags(QModelIndexList() << m_message, "\\Flagged", isMarkedFlagged() ? Imap::Mailbox::FLAG_REMOVE : Imap::Mailbox::FLAG_ADD);
}

void MessageModel::setMessageModelIndex(QModelIndex index)
{
    setMessage(index);
}

QModelIndex MessageModel::getModelIndexFromPartId(QString partId)
{
    QModelIndex item = messageFirstChildIndex();
    QStringList separated = partId.split('.');
    for (QStringList::const_iterator it = separated.constBegin(); it != separated.constEnd(); ++it) {
        bool ok;
        uint number = it->toUInt(&ok);
        Q_ASSERT(ok);
        // Normal path: descending down and finding the correct part
        QModelIndex child = item.child(0, 0);
        if (child.isValid() && child.data(Imap::Mailbox::RolePartIsTopLevelMultipart).toBool())
            item = child;
        item = item.child(number - 1, 0);
        Q_ASSERT(item.isValid());
    }
    return item;
}

QList<QByteArray> MessageModel::replyReferences() const
{
    if(!m_message.isValid()) {
        return QList<QByteArray>();
    }
    QList<QByteArray> res = references();
    res.append(messageId());
    return res;
}

void MessageModel::setMessage(const QString &mboxName, const uint uid)
{
    if (mboxName.isEmpty())
        return;
    Q_ASSERT(m_imapModel);
    setMessage(m_imapModel->messageIndexByUid(mboxName, uid));
}

void MessageModel::setMessage(const QModelIndex &message)
{
    Q_ASSERT(message.isValid() || message.model() == m_imapModel);
    m_message = message;
    emit messageChanged();
}

void MessageModel::messageClosed()
{
    m_message = QModelIndex();
    emit messageChanged();
}

void MessageModel::handleModelDataChanged(const QModelIndex &topLeft, const QModelIndex &btmRight)
{
    Q_ASSERT(topLeft.row() == btmRight.row());
    Q_ASSERT(topLeft.parent() == btmRight.parent());
    Q_ASSERT(topLeft.model() == btmRight.model());

    if (m_message.isValid() && topLeft == m_message)
        emit flagsChanged();
}

QObject *MessageModel::imapModel()
{
    return m_imapModel;
}

void MessageModel::setImapModel(QObject *imapModel)
{
    if (!imapModel) {
        return;
    }
    m_imapModel = static_cast<Imap::Mailbox::Model *>(imapModel);
    initialize();
}

void MessageModel::setModel(Imap::Mailbox::Model *model)
{
    m_imapModel = model;
    initialize();
}

uint MessageModel::uid()
{
    return m_message.data(Imap::Mailbox::RoleMessageUid).toUInt();
}

QString MessageModel::mailbox()
{
    return m_mailbox;
}

void MessageModel::setMailbox(QString mailbox)
{
    m_mailbox = mailbox;
    emit mailboxChanged();
}

QModelIndex MessageModel::mainTextPartIndex()
{
    QModelIndex idx;
    QString partMessage;
    Dekko::Utils::FindTextPart::findTextPartOfMessage(messageIndex(), idx, partMessage, 0);
    return idx;
}

bool MessageModel::isMarkedDeleted() const
{
    return m_message.data(Imap::Mailbox::RoleMessageIsMarkedDeleted).toBool();
}

bool MessageModel::isMarkedRead() const
{
    return m_message.data(Imap::Mailbox::RoleMessageIsMarkedRead).toBool();
}

bool MessageModel::isMarkedForwarded() const
{
    return m_message.data(Imap::Mailbox::RoleMessageIsMarkedForwarded).toBool();
}

bool MessageModel::isMarkedReplied() const
{
    return m_message.data(Imap::Mailbox::RoleMessageIsMarkedReplied).toBool();
}

bool MessageModel::isMarkedRecent() const
{
    return m_message.data(Imap::Mailbox::RoleMessageIsMarkedRecent).toBool();
}

bool MessageModel::isMarkedFlagged() const
{
    return m_message.data(Imap::Mailbox::RoleMessageIsMarkedFlagged).toBool();
}

}
}
