/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of the Dekko

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3 or any later version
   accepted by the membership of KDE e.V. (or its successor approved
   by the membership of KDE e.V.), which shall act as a proxy
   defined in Section 14 of version 3 of the license.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "AccountsListModel.h"
#include <QSettings>
#include <QDebug>
#include <QFile>
#include <QUuid>
#include "app/Accounts/AccountsManager.h"
#include "app/Accounts/AccountUtils.h"

namespace Dekko
{
namespace Models
{

AccountsListModel::AccountsListModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{
}

QAbstractItemModel *AccountsListModel::srcModel() const
{
    return sourceModel();
}

void AccountsListModel::setSrcModel(QAbstractItemModel *arg)
{
    if (sourceModel() != arg) {
        setSourceModel(arg);
        connect(sourceModel(), SIGNAL(rowsInserted(QModelIndex,int,int)), this, SIGNAL(countChanged()));
        connect(sourceModel(), SIGNAL(rowsRemoved(QModelIndex,int,int)), this, SIGNAL(countChanged()));
        emit srcModelChanged();
    }
}

int AccountsListModel::count() const
{
    return rowCount();
}

bool AccountsListModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex index = sourceModel()->index(source_row, 0, source_parent);
    switch (filterRole()) {
    case FILTER_IMAP:
    case FILTER_IMAP_SMTP:
        return index.data(Accounts::AccountsManager::AccountTypeRole) == Accounts::Account::IMAP ||
                index.data(Accounts::AccountsManager::AccountTypeRole) == Accounts::Account::IMAP_SMTP ||
                index.data(Accounts::AccountsManager::AccountTypeRole) == Accounts::Account::ONLINE_ACCOUNT;
    case FILTER_SMTP:
        return index.data(Accounts::AccountsManager::AccountTypeRole) == Accounts::Account::SMTP;
    case FILTER_INVALID:
        return false;
    default:
        return false;
    }
}

}
}
