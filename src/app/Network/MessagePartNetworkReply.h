#ifndef MSGPARTNETWORKREPLY_H
#define MSGPARTNETWORKREPLY_H

#include <QBuffer>
#include "MsgPartNetworkAccessManager.h"
#include <QModelIndex>
#include <QNetworkReply>

namespace Dekko
{
namespace Network
{

/** @short Qt-like access to one MIME message part */
class MessagePartNetworkReply : public QNetworkReply
{
    Q_OBJECT
public:
    MessagePartNetworkReply(MsgPartNetworkAccessManager *parent, const QPersistentModelIndex &part, bool requireFormatting = false);
    virtual void abort();
    virtual void close();
    virtual qint64 bytesAvailable() const;
public slots:
    void slotModelDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    void slotMyDataChanged();
protected:
    virtual qint64 readData(char *data, qint64 maxSize);
private:
    void disconnectBufferIfVanished() const;

    QPersistentModelIndex part;
    mutable QBuffer buffer;
    QByteArray *formattedBufferContent;
    bool requireFormatting;

    MessagePartNetworkReply(const MessagePartNetworkReply &); // don't implement
    MessagePartNetworkReply &operator=(const MessagePartNetworkReply &); // don't implement
};

}
}

#endif // MSGPARTNETWORKREPLY_H
