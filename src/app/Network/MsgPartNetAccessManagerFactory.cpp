/* Copyright (C) 2014-2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "MsgPartNetAccessManagerFactory.h"

namespace Dekko
{
namespace Network
{

void MsgPartNetAccessManagerFactory::setAccountsManager(QObject *manager)
{
    m_accountsManager = static_cast<Accounts::AccountsManager *>(manager);
    Q_ASSERT(m_accountsManager);
}

QNetworkAccessManager *MsgPartNetAccessManagerFactory::create(QObject *parent)
{
    MsgPartNetworkAccessManager *mpnam = new MsgPartNetworkAccessManager(parent);
    mpnam->setAccountsManager(m_accountsManager);
    connect(mpnam, SIGNAL(destroyed(QObject*)), this, SLOT(handleManagerDestroyed(QObject *)));
    m_mpnamList.append(mpnam);
    return mpnam;
}

void MsgPartNetAccessManagerFactory::handleManagerDestroyed(QObject *destroyedObject)
{
    m_mpnamList.removeOne(qobject_cast<MsgPartNetworkAccessManager *>(destroyedObject));
}

}
}
