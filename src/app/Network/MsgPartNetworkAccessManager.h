#ifndef MSGPARTNETWORKACCESSMANAGER_H
#define MSGPARTNETWORKACCESSMANAGER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QPersistentModelIndex>
#include <QUrlQuery>
#include <QNetworkRequest>
#include <QNetworkReply>
#include "3rdParty/trojita/Imap/Model/Model.h"
#include "3rdParty/trojita/Imap/Model/MailboxTree.h"
#include "app/Accounts/AccountsManager.h"
#include "app/Accounts/Account.h"

namespace Dekko
{

namespace Network
{

class MsgPartNetworkAccessManager : public QNetworkAccessManager
{
    Q_OBJECT
public:
    explicit MsgPartNetworkAccessManager(QObject *parent = 0);

    QModelIndex indexFromQuery(const QUrlQuery &query);
    static QModelIndex pathToPart(const QModelIndex &message, const QString &path);
    static QModelIndex cidToPart(const QModelIndex &rootIndex, const QByteArray &cid);

    QString translateToSupportedMimeType(const QString &originalMimeType) const;
    void registerMimeTypeTranslation(const QString &originalMimeType, const QString &translatedMimeType);
    bool hostInAllowedUrls(const QString &host);
    bool schemeInAllowedSchemes(const QString &scheme);

signals:

public slots:
    void setAccountsManager(QObject *manager);

protected:
    virtual QNetworkReply *createRequest(Operation op, const QNetworkRequest &req, QIODevice *outgoingData=0);

private:
    QPointer<Accounts::AccountsManager> m_accountsManager;
    QPersistentModelIndex m_messageIndex;
    QMap<QString, QString> m_mimeTypeFixups;

};
}
}

#endif // MSGPARTNETWORKACCESSMANAGER_H
