/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ONLINEACCOUNTSSERVICE_H
#define ONLINEACCOUNTSSERVICE_H

#include <QObject>
#include <QSharedPointer>
#include <accounts-qt5/Accounts/Account>
#include <accounts-qt5/Accounts/Application>
#include <accounts-qt5/Accounts/AccountService>
#include "app/Accounts/Account.h"

class OASharedManager
{
public:
    static QSharedPointer<Accounts::Manager> instance();
};

class OnlineAccountsService : public QObject
{
    Q_OBJECT
    typedef QList<Dekko::Accounts::Account *> DekkoAccountList;
    typedef QList<Accounts::AccountService *> OAAccountServiceList;

public:
    explicit OnlineAccountsService(QObject *parent = 0);
    ~OnlineAccountsService();

signals:
    // Get's emitted when an account in USSOA is enabled
    void accountEnabled(const QString &accountId);
    // Get's emitted when an account in USSOA is disabled
    void accountDisabled(const QString &accountId);
    // Get's emitted when an new account is created but we don't know
    // about it locally. i.e brand new account
    void newAccountEnabled(const uint &id);
    // This is slightly different to the above, in that the account
    // was created while dekko was open, this comes from the manager->accountCreated signal
    // which will be a result of being in the setupwizard, the accountsManager
    // needs to ignore this signal and only listen on the above newAccountEnabled
    void newAccountCreated(const uint &id);
    void accountRemoved(const QString &accountId);

public slots:
    bool checkAccountStillEnabled(const uint &accountId);
    bool checkAccountStillExists(const uint &accountId);
    void updateAllowedServices();

private slots:
    void handleAccountCreated(Accounts::AccountId accountId);
    void handleAccountRemoved(Accounts::AccountId accountId);
    void handleEnabledChanged(bool enabled);
    void handleAppStateChanged(Qt::ApplicationState state);
private:

    void watchServices(OAAccountServiceList serviceList);
    bool checkForLocalAccount(Accounts::AccountService *service);
    QString getDekkoAccountIdForService(Accounts::AccountService *service);
    DekkoAccountList getDekkoAccounts();
    OAAccountServiceList getAllowedServices();
    OAAccountServiceList getAllServices();

    QSharedPointer<Accounts::Manager> m_oaManager;
    Accounts::Application m_application;
    DekkoAccountList m_localAccounts;
    OAAccountServiceList m_allServices;
    OAAccountServiceList m_allowedServices;

};

#endif // ONLINEACCOUNTSSERVICE_H
