/* Copyright (C) 2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMBINEDFLATTENCONTACTFILTERMODEL_H
#define COMBINEDFLATTENCONTACTFILTERMODEL_H

#include "3rdParty/RowsJoinerProxy.h"

namespace Dekko {
namespace Contacts {

class CombinedFlattenContactModel: public RowsJoinerProxy
{
    Q_OBJECT
    Q_PROPERTY(QAbstractItemModel *savedContactModel READ savedContactModel WRITE setSavedContactModel)
    Q_PROPERTY(QAbstractItemModel *recentContactModel READ recentContactModel WRITE setRecentContactModel)
public:
    explicit CombinedFlattenContactModel(QObject *parent = 0);

    QHash<int, QByteArray> roleNames() const;

    QAbstractItemModel *savedContactModel();
    void setSavedContactModel(QAbstractItemModel *savedContactModel);
    QAbstractItemModel *recentContactModel();
    void setRecentContactModel(QAbstractItemModel *recentContactModel);

private:
    QAbstractItemModel *m_savedContactModel;
    QAbstractItemModel *m_recentContactModel;
};

}
}

#endif // COMBINEDFLATTENCONTACTFILTERMODEL_H
