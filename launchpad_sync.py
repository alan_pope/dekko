#!/usr/bin/python3
from os.path import isdir, join, abspath, dirname
from sh import bzr, cd, git, rm, ErrorReturnCode

bzr_repo = join(dirname(abspath(__file__)), 'bzr-repo')
bzr_branch = 'lp:~/+junk/sync'
bzr_branch_devel = 'lp:dekko/0.5'

def cleanup():
    if isdir(bzr_repo):
        print("Removing old bzr repo: " + bzr_repo)
        rm(bzr_repo, r=True, f=True)

def push_branch_to_target(target, directory):
    bzr.push(target, overwrite=True, directory=directory)

def export_branch(src):

    git.checkout(src)
    git.reset('--hard', src)
    cleanup()

    print("Creating new bzr repo: " + bzr_repo)
    bzr('init-repo', 'bzr-repo')

    print("Moving to new bzr repo")
    cd(bzr_repo)

    print("Exporting git to bzr")
    bzr(git('-C', '../', 'fast-export', M=True, all=True), 'fast-import', '-')

def syncWithLaunchpad():
    # Needs changing to sync with lp:dekko once move is finished

    git.fetch(all=True)
    export_branch('devel')
    push_branch_to_target(bzr_branch_devel, 'devel')
    cleanup()
    # export_branch('origin/master')
    # push_branch_to_target(bzr_branch, 'trunk')
    # cleanup()
    print("All synced to: " + bzr_branch_devel)
    print("Now we just gotta wait for the translation wizards to do their thing")

if __name__ == "__main__":
    syncWithLaunchpad()
