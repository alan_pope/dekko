#include "TestViewSettings.h"
#include <QScopedPointer>
#include <QFile>
#include <QSignalSpy>
#include <QDebug>
#include "app/Settings/SettingsFileBase.h"

#define TEST_SETTINGS_FILE_PATH "./viewSettings.json"
#define TEST_PATH "default"

namespace Dekko
{
namespace Settings
{

void TestViewSettings::testSetPath()
{
    Dekko::Settings::SettingsFileBase *viewSettings = new Dekko::Settings::SettingsFileBase;
    Dekko::Settings::ViewSettings::setViewSettingsFile(viewSettings);
    viewSettings->setPath(QLatin1String(TEST_SETTINGS_FILE_PATH));
    m_settings = new ViewSettings(this);
    QSignalSpy spy(m_settings, SIGNAL(pathChanged()));
    m_settings->setPath(QLatin1String(TEST_PATH));
    QCOMPARE(spy.count(), 1);
    QCOMPARE(m_settings->path(), QString(TEST_PATH));
    QVERIFY(m_settings->isValid());
}

void TestViewSettings::testSaveString()
{
    Dekko::Settings::SettingsFileBase *viewSettings = new Dekko::Settings::SettingsFileBase;
    Dekko::Settings::ViewSettings::setViewSettingsFile(viewSettings);
    viewSettings->setPath(QLatin1String(TEST_SETTINGS_FILE_PATH));
    m_settings = new ViewSettings();
    m_settings->setPath(TEST_PATH);
    QSignalSpy spy(m_settings, SIGNAL(dataChanged()));
    m_settings->write(QLatin1String("value"), QString("foobar"));
    QCOMPARE(spy.count(), 1);
    QStringList keys = m_settings->data().keys();
    QCOMPARE(keys.size(), 1);
    QString result = m_settings->read(QLatin1String("value")).toString();
    QCOMPARE(result, QString("foobar"));
}

void TestViewSettings::testSaveInt()
{
    Dekko::Settings::SettingsFileBase *viewSettings = new Dekko::Settings::SettingsFileBase;
    Dekko::Settings::ViewSettings::setViewSettingsFile(viewSettings);
    viewSettings->setPath(QLatin1String(TEST_SETTINGS_FILE_PATH));
    m_settings = new ViewSettings();
    m_settings->setPath(TEST_PATH);
    QSignalSpy spy(m_settings, SIGNAL(dataChanged()));
    m_settings->write(QLatin1String("value"), 100);
    QCOMPARE(spy.count(), 1);
    QStringList keys = m_settings->data().keys();
    QCOMPARE(keys.size(), 1);
    int result = m_settings->read(QLatin1String("value")).toInt();
    QCOMPARE(result, 100);
}

void TestViewSettings::testSaveBool()
{
    Dekko::Settings::SettingsFileBase *viewSettings = new Dekko::Settings::SettingsFileBase;
    Dekko::Settings::ViewSettings::setViewSettingsFile(viewSettings);
    viewSettings->setPath(QLatin1String(TEST_SETTINGS_FILE_PATH));
    m_settings = new ViewSettings();
    m_settings->setPath(TEST_PATH);
    QSignalSpy spy(m_settings, SIGNAL(dataChanged()));
    m_settings->write(QLatin1String("value"), true);
    QCOMPARE(spy.count(), 1);
    QStringList keys = m_settings->data().keys();
    QCOMPARE(keys.size(), 1);
    bool result = m_settings->read(QLatin1String("value")).toBool();
    QCOMPARE(result, true);
}

void TestViewSettings::testKeys()
{
    m_settings = new ViewSettings();
    QCOMPARE(m_settings->keys().size(), 2);
}

void TestViewSettings::cleanup()
{
    delete m_settings;
    QFile(TEST_SETTINGS_FILE_PATH).remove();
}

void TestViewSettings::cleanupTestCase()
{
    if (QFile(TEST_SETTINGS_FILE_PATH).exists()) {
        QFile(TEST_SETTINGS_FILE_PATH).remove();
    }
}

}
}
QTEST_MAIN(Dekko::Settings::TestViewSettings)
