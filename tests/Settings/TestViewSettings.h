#ifndef TESTVIEWSETTINGS_H
#define TESTVIEWSETTINGS_H

#include <QObject>
#include <QTest>
#include "app/Settings/ViewSettings.h"
namespace Dekko
{
namespace Settings
{
class TestViewSettings : public QObject
{
    Q_OBJECT

private slots:
    void cleanup();
    void cleanupTestCase();
    void testSetPath();
    void testSaveString();
    void testSaveInt();
    void testSaveBool();
    void testKeys();

private:
    Dekko::Settings::ViewSettings *m_settings;
};
}
}

#endif // TESTVIEWSETTINGS_H
