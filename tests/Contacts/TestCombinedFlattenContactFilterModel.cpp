#include "TestCombinedFlattenContactFilterModel.h"

#include <QTest>

namespace Dekko {
namespace Contacts {

void TestCombinedFlattenContactFilterModel::before()
{
    m_flattenContactModel1 = new FlattenContactModel(this);
    m_flattenContactModel2 = new FlattenContactModel(this);
    m_combinedFlattenContactModel = new CombinedFlattenContactModel(this);
    m_combinedFlattenContactModel->setSavedContactModel(m_flattenContactModel1);
    m_combinedFlattenContactModel->setRecentContactModel(m_flattenContactModel2);
    m_combinedFlattenContactFilterModel = new CombinedFlattenContactFilterModel(this);
    m_combinedFlattenContactFilterModel->setSourceModel(m_combinedFlattenContactModel);
}

void TestCombinedFlattenContactFilterModel::after()
{
    delete m_combinedFlattenContactFilterModel;
    delete m_combinedFlattenContactModel;
    delete m_flattenContactModel1;
    delete m_flattenContactModel2;
}

void TestCombinedFlattenContactFilterModel::testRemovingDuplicateEmails()
{
    before();
    m_flattenContactModel1->addContact("Contact1", "duplicate@email.com");
    m_flattenContactModel2->addContact("Contact2", "duplicate@email.com");
    m_flattenContactModel2->addContact("Contact3", "notduplicate@email.com");
    QVERIFY(m_combinedFlattenContactFilterModel->rowCount() == 2);
    after();
}

}
}

QTEST_MAIN(Dekko::Contacts::TestCombinedFlattenContactFilterModel)
