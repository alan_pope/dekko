#ifndef TESTCOMBINEDFLATTENCONTACTFILTERMODEL_H
#define TESTCOMBINEDFLATTENCONTACTFILTERMODEL_H

#include "app/Contacts/CombinedFlattenContactFilterModel.h"
#include "app/Contacts/CombinedFlattenContactModel.h"
#include "app/Contacts/FlattenContactModel.h"

namespace Dekko {
namespace Contacts {

class TestCombinedFlattenContactFilterModel: public QObject
{
    Q_OBJECT
private:
    Dekko::Contacts::CombinedFlattenContactFilterModel *m_combinedFlattenContactFilterModel;
    Dekko::Contacts::CombinedFlattenContactModel *m_combinedFlattenContactModel;
    Dekko::Contacts::FlattenContactModel *m_flattenContactModel1;
    Dekko::Contacts::FlattenContactModel *m_flattenContactModel2;

    void before();
    void after();
private slots:
    void testRemovingDuplicateEmails();
};

}
}
#endif // TESTCOMBINEDFLATTENCONTACTFILTERMODEL_H
