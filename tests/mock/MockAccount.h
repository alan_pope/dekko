#ifndef MOCKACCOUNT_H
#define MOCKACCOUNT_H

#include <QObject>
namespace Dekko
{
namespace Accounts
{
class Account : public QObject
{
    Q_OBJECT
public:
    explicit Account(QObject *parent = 0);

    QString accountId() const;
    QObject *mailboxModel() const;

signals:

public slots:

};
}
}
#endif // MOCKACCOUNT_H
