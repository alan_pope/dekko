#include "MockAccount.h"

namespace Dekko
{
namespace Accounts
{
Account::Account(QObject *parent) :
    QObject(parent)
{
}

QString Account::accountId() const
{
    return QString("mock-account");
}

QObject *Account::mailboxModel() const
{
    return new QObject();
}

}
}
