#include "MockMailboxListModel.h"
#include "Imap/Model/ItemRoles.h"
#include <cassert>

MockMailboxListModel::MockMailboxListModel()
    : QAbstractItemModel()
{
}

void MockMailboxListModel::setMockData(QList<MockMessage> list)
{
    m_messageList = list;
}

int MockMailboxListModel::rowCount(const QModelIndex &parent) const
{
    return m_messageList.length();
}

QVariant MockMailboxListModel::data(const QModelIndex &index, int role) const
{
    return const_cast<MockMailboxListModel *>(this)->dataW(index, role);
}

QVariant MockMailboxListModel::dataW(const QModelIndex &index, int role)
{
    if (role == Imap::Mailbox::RoleTotalMessageCount) {
        return m_messageList.length();
    }
    MockMessage m = m_messageList[index.row()];
    switch (role) {
    case Imap::Mailbox::RoleMessageUid:
        return m.uid;
    case Imap::Mailbox::RoleMessageSubject:
        if (!m.fetched) {
            m_messageList[index.row()].fetched = true;
            const_cast<MockMailboxListModel *>(this)->dataChanged(index, index);
        }
        return m.subject;
    case Imap::Mailbox::RoleMessageDate:
        return m.time;
    case Imap::Mailbox::RoleIsFetched:
        return m.fetched;
    }
    return QVariant();
}

QModelIndex MockMailboxListModel::index(int row, int column, const QModelIndex &parent) const
{
    return createIndex(row, column, (void *) 0);
}

QModelIndex MockMailboxListModel::parent(const QModelIndex &child) const
{
    return createIndex(0, 0, (void *) 0);
}

int MockMailboxListModel::columnCount(const QModelIndex &parent) const
{
    return 1;
}
