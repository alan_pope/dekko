#ifndef MOCKMAILBOXLISTMODEL_H
#define MOCKMAILBOXLISTMODEL_H

#include <QAbstractItemModel>
#include <QDateTime>
struct MockMessage {
    int uid;
    QString subject;
    QDateTime time;
    bool fetched;
    MockMessage(int uid, QString subject, QDateTime time, bool fetched)
        : uid(uid), subject(subject), time(time), fetched(fetched) { }
};

class MockMailboxListModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    MockMailboxListModel();

    void setMockData(QList<MockMessage> list);

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant dataW(const QModelIndex &index, int role);
    virtual QModelIndex index(int row, int column,
                              const QModelIndex &parent = QModelIndex()) const;
    virtual QModelIndex parent(const QModelIndex &child) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;

private:

    void emitDataChanged(QModelIndex idx);
    QList<MockMessage> m_messageList;
};

#endif // MOCKMAILBOXLISTMODEL_H
