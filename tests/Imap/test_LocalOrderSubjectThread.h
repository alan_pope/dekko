#include "3rdParty/trojita/Imap/Parser/ThreadingNode.h"
#include "3rdParty/trojita/Imap/Tasks/LocalOrderedSubjectThreadTask.h"
#include "../mock/MockMailboxListModel.h"

namespace Imap {
namespace Mailbox {

class TestLocalOrderedSubjectThreadTask: public QObject {
    Q_OBJECT

private slots:
    void testThreadForNumberOfMailsImp();
    void testWorkflow();

    void handleThreadingAvailable(QModelIndex,QByteArray,QStringList,QVector<Imap::Responses::ThreadingNode>);

private:

    void runWorkflowTest(QList<MockMessage> input, QVector<Imap::Responses::ThreadingNode> result, QDateTime since);

    LocalOrderedSubjectThreadTask* m_task;
    QVector<Responses::ThreadingNode> m_map;
};

}
}
