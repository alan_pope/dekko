import QtQuick 2.4
import Ubuntu.Components 1.2

// The MainStage will only show the left stage when
// When on small formactors.
StageArea {
    id: stage
    baseUrl: Qt.resolvedUrl("../MessageListView/MessageListPage.qml")
}
