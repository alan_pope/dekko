import QtQuick 2.4
import Ubuntu.Components 1.2

// A stage area is a root container used for an area of the MainStage
// A StageArea has it's own internal page stack and should be used
// by all child pages of this StageArea and can be accessed by it's "internalStack" id
Item {

    // Set a page url to load on creation
    property var baseUrl: undefined
    // Set a base component to load on creation
    property Component baseComponent: null
    // Function to push a page onto the stack
    function push(item, props) {
        internalStack.push(item, props)
    }

    // Since our Pages are Item based and not Page
    // We can safely put nested pagestacks without warnings.
    // All left stage pages should use this internalStack rather than rootPageStack
    PageStack {
        id: internalStack
        function openSettings(viewToReturnTo) {
            clear()
            push(Qt.resolvedUrl("../Settings/global/GlobalSettingsListView.qml"), {previousView: viewToReturnTo})
        }
        anchors.fill: parent // make sure our stack takes the full size

        Component.onCompleted: {
            if (baseComponent !== null) {
                console.log("PUSHING COMPONENT")
                push(baseComponent)
            } else if (baseUrl !== undefined) {
                console.log("PUSHING URL")
                push(baseUrl)
            } else {
                console.log("[Stage Area] No component given: ignoring!")
            }
        }
    }
}
