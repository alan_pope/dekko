import QtQuick 2.4
import Ubuntu.Components 1.2
import DekkoCore 0.2
import "../UCSComponents"
import "../Composer"
import "../Utils"

PageWithBottomEdge {
    id: mainStage

    anchors.fill: parent
    property Component leftStage: StageLeft{}
    property Component rightStage: StageRight{}
    property alias showDivider: divider.visible

    // We place the layouts inside this item that way
    // we can have a smart bottom edge that doesn't need to be on every page
    // and will constantly sit above the layouts even as we push new pages etc
    Item {
        anchors.fill: parent

        Loader {
            id: leftLoader
            active: leftStage !== null
            sourceComponent: leftStage
            anchors {
                left: parent.left
                top: parent.top
                bottom: parent.bottom
            }
            width: (parent.width > units.gu(90) && !dekko.viewState.isSmallFF) ? units.gu(40) : parent.width
        }

        Rectangle {
            id: divider
            visible: true // default to true
            anchors {
                left: leftLoader.right
                top: parent.top
                bottom: parent.bottom
            }
            width: visible ? units.dp(1) : 0
            color: Style.header.text
        }
        Loader {
            id: rightLoader
            active: rightStage !== null && !dekko.viewState.isSmallFF
            sourceComponent: rightStage
            anchors {
                left: divider.right
                top: parent.top
                bottom: parent.bottom
                right: parent.right
            }
        }
    }

    bottomEdgePageComponent: ComposePage {
        replyMode: ReplyMode.REPLY_NONE
        pageTitle: qsTr("New Message")
        isOpen: bottomEdgeExpanded
    }
    bottomEdgeEnabled: true
    hintIconColor: Style.common.text
    //    bottomEdgeTitle: qsTr("Compose")
}
