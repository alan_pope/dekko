/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of the dekko

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3 or any later version
   accepted by the membership of KDE e.V. (or its successor approved
   by the membership of KDE e.V.), which shall act as a proxy
   defined in Section 14 of version 3 of the license.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.4
import Ubuntu.Components 1.2
import DekkoCore 0.2
import "../Components"
import "../Utils/Utils.js" as Utils

Item {
    id: header

    height: units.gu(10.075)
    anchors {
        left: parent.left
        right: parent.right
        top: parent.top
    }

    Component.onCompleted: setup()

    Connections {
        target: dekko.currentAccount.msgModel
        onMessageChanged: setup()
    }

    Connections {
        target: dekko
        onCurrentAccountChanged: setup()
    }

    function setup() {
        console.log("Setup Called")
        if (dekko.currentAccount.msgModel.hasValidIndex) {
            var fromName = Utils.formatMailAddresses(dekko.currentAccount.msgModel.from, true)
            inner_avatar.avText = Style.avatar.getInitialsForName(fromName)
            inner_avatar.avColor = Utils.getIconColor(fromName, Style.avatar.avatarColorList)
            inner_avatar.avEmail = Utils.formatPlainArrayMailAddresses(dekko.currentAccount.msgModel.from, false, true)[0]
            fromLabel.text = fromName
            subjectLabel.text = dekko.currentAccount.msgModel.subject
            // TODO: come up with some logic similar to TreeItemMessage::fuzzyDate
            // so we get Today, Yesterday etc then long date for anything > 7 days
            dateLabel.text = Qt.formatDateTime(dekko.currentAccount.msgModel.date, "ddd hh:mm")
        }
    }

    Item {
        id: inner_avatar
        property var avColor
        property string avText
        property var avEmail
        height: parent.height - units.gu(3)
        anchors {
            left: parent.left
            leftMargin: units.gu(1)
            verticalCenter: parent.verticalCenter
        }
        visible: dekko.currentAccount.msgModel.hasValidIndex
        width: height

        Component {
            id: component_inner_av
            CachedImage {
                id: avatarCircle
                height: parent.height
                width: height
                anchors.centerIn: parent
                color: inner_avatar.avColor
                name: "avatar-circle"
                Label {
                    id: avatarLabel
                    color: "#FFFFFF"
                    visible: avatarCircle.status === Image.Ready
                    anchors.centerIn: parent
                    text: inner_avatar.avText
                    fontSize: "x-large"
                }
            }
        }
        Loader {
            id: loader_inner_av
            active: !inner_name.visible
            sourceComponent: component_inner_av
            anchors.fill: parent
        }
        CircleGravatar {
            id: inner_name
            clip: true
//            emailAddress: inner_avatar.avEmail
            anchors.fill: parent
            width: parent.width
            height: parent.height
            visible: status === Image.Ready
        }
    }

    Label {
        id: fromLabel
        visible: dekko.currentAccount.msgModel.hasValidIndex
        anchors {
            left: inner_avatar.right
            leftMargin: units.gu(1)
            top: parent.top
            topMargin: units.gu(2)
            right: dateLabel.left
            rightMargin: units.gu(1)
        }
        clip: true
        elide: Text.ElideRight
        fontSize: "large"
    }

    Label {
        id: subjectLabel
        visible: dekko.currentAccount.msgModel.hasValidIndex
        anchors {
            left: inner_avatar.right
            leftMargin: units.gu(1)
            top: fromLabel.bottom
            topMargin: units.gu(1)
            right: starButton.left
            rightMargin: units.gu(1)
        }
        clip: true
        elide: Text.ElideRight
    }

    Label {
        id: dateLabel
        anchors {
            right: parent.right
            top: parent.top
            topMargin: units.gu(2)
            rightMargin: units.gu(1)
        }
        visible: dekko.currentAccount.msgModel.hasValidIndex
    }

    HeaderButton {
        id: starButton
        anchors {
            right: deleteButton.left
            bottom: parent.bottom
            bottomMargin: units.gu(1)
        }
        visible: dekko.currentAccount.msgModel.hasValidIndex
        iconColor: dekko.currentAccount.msgModel.isMarkedFlagged ? Style.actions.starred : Style.header.icons
        action: Action {
            iconSource: dekko.currentAccount.msgModel.isMarkedFlagged ? "qrc:///actions/starred.svg" : "qrc:///actions/favorite-unselected.svg"
            onTriggered: dekko.currentAccount.msgModel.markMessageFlagged()
        }
    }

    HeaderButton {
        id: deleteButton
        anchors {
            right: parent.right
            bottom: parent.bottom
            bottomMargin: units.gu(1)
        }
        visible: dekko.currentAccount.msgModel.hasValidIndex
        action: Action {
            iconName: "delete"
            onTriggered: {
                dekko.currentAccount.msgModel.markMessageDeleted()
                dekko.currentAccount.msgModel.messageClosed()
            }
        }
    }

    Rectangle {
        id: btmMargin
        anchors {
            left: parent.left
            right: parent.right
            bottom: header.bottom
        }
        color: Style.header.text
        height: units.gu(0.075)
        visible: true
    }
}
