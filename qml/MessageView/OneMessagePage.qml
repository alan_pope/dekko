/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of the dekko

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3 or any later version
   accepted by the membership of KDE e.V. (or its successor approved
   by the membership of KDE e.V.), which shall act as a proxy
   defined in Section 14 of version 3 of the license.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import Ubuntu.Components.Popups 1.0
import UserMetrics 0.1
import TrojitaCore 0.1
import DekkoCore 0.2
import "../Components"
import "../Dialogs"
import "../Utils/Utils.js" as Utils
import "../UCSComponents"
import "../Composer"

Page {
    id: oneMessagePage

    property var uid: messageModel.uid
    property var mailboxName: messageModel.mailbox
    readonly property var messageIndex: messageModel.messageIndex
    property bool hasAttachments: false
    property bool displayingHTML: false
    property var messageModel: dekko.currentAccount.msgModel

    //Composer properties
    property string fromName: qsTr("Reply to %1").arg(Utils.formatPlainArrayMailAddresses(messageModel.from, true, false))
    property string replyMessage: ""// bind in PlainTextPart
    property var messageListModel
    property bool editable: true
    property string partId // store the part of the embedded message to display
    property bool showBack: false

    flickable: null

    Connections {
        target: dekko.viewState
        onStateChanged: {
            if (showBack) {
                rootPageStack.pop()
            }
        }
    }

    MessageHeader {
        id: envelopeHeader
    }

    DekkoHeader {
        id: header
        title: qsTr("Message")
        anchors {
            left: parent.left
            top: parent.top
            right: parent.right
        }
        visible: showBack
        backAction: showBack ? backAction : undefined
        primaryAction: editable ? deleteAction : null
        secondaryActions: editable ? [prevMsgAction, nextMsgAction] : null
    }



    Action {
        id: backAction
        enabled: !htmlView.isScrolling
        iconName: "back"
        onTriggered: {
            dekko.currentAccount.msgModel.messageClosed()
            rootPageStack.pop()
        }
    }

    function load() {
        if (!dekko.currentAccount.msgModel.hasValidIndex) {
            htmlView.url = "about:blank"
            return;
        }

        if (editable) {
            startMarkAsReadTimerPerSetting();
        }
        dekko.currentAccount.msgModel.openMessage();
//        msgPartNetAccessManagerFactory.setMessageIndex(messageModel.messageIndex)
        msgPartNAM.setModelMessage(dekko.currentAccount.msgModel.messageIndex)
        if (partId) {
            messageBuilder.msgIndex = dekko.currentAccount.msgModel.getModelIndexFromPartId(partId)
        } else {
            messageBuilder.msgIndex = dekko.currentAccount.msgModel.messageIndex
        }
        messageBuilder.buildMessage()
    }

    function startMarkAsReadTimerPerSetting() {
        switch (GlobalSettings.preferences.markAsRead) {
        case Preferences.NEVER:
            break;
        case Preferences.AFTER_X_SECS:
            markAsReadTimer.interval = GlobalSettings.preferences.markAsReadAfter * 1000
            markAsReadTimer.start()
            break;
        case Preferences.IMMEDIATELY:
            markAsReadTimer.start()
            break;
        }
    }
    onUidChanged: console.log("uid changed: ", uid)
    onMailboxNameChanged: console.log("mailboxname changed: ", mailboxName)

    ViewSettings {
        id: testOne
        path: dekko.currentAccount.accountId
//        onPathChanged: {
//            if (dekko.viewState.isSmallFF) {
//                return
//            }
//            console.log("Setting uid & mailbox to invalid")
//            uid = -1
//            mailboxName = ""
//        }

//        onDataChanged: {
////            if (dekko.viewState.isSmallFF) {
////                return;
////            }
//////            console.log("SETTING MSG FROM VIEW SETTINGS")
////////            mailboxName = data.current_mailbox
////            if (data.selected_uid !== -1) {
////                console.log("uid not -1 so loading message")
////                load()
////            }
//        }
    }

    Connections {
        target: dekko.currentAccount.msgModel
        onMessageChanged: load()
    }

    Connections {
        target: dekko
        onCurrentAccountChanged: {
            htmlView.url = "about:blank"
            load()
        }
    }

    Component.onCompleted: {
        if (dekko.viewState.isSmallFF) {
            console.log("TRYING TO LOAD MESSAGE")
            load();
        }
    }

    MessageBuilder {
        id: messageBuilder
        // This property protects us from the network connection being reset
        // We want to only allow the message to be loaded once, unless goNext/Prev is called
        // Which will reset this to allow the next message to load.
        property bool allowLoading: true
        mbox: dekko.currentAccount.msgListModel.mailboxName
        accountId: dekko.currentAccount.accountId
        showEnvelope: showBack
        messageModel: dekko.currentAccount.msgModel
        preferPlainText: GlobalSettings.preferences.preferPlainText
        onMessageChanged: {
            if (allowLoading) {
                htmlView.loadHtml(message, Qt.resolvedUrl("."))
                // We've loaded so block unless the uid changes via goNext/Prev
                if (dekko.viewState.isSmallFF) {
                    allowLoading = false;
                }
            }
        }
    }
    DekkoWebView {
        id: htmlView
        property bool isScrolling
        onContentYChanged: {
            isScrolling = true
            webviewTimer.start()
        }
        onContentXChanged: {
            isScrolling = true
            webviewTimer.start()
        }
        anchors {
            top: showBack ? header.bottom : envelopeHeader.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        onOpenAttachment: attachmentDownloader.fetchPart(partId);
        onOpenEmbeddedMessage: {
            rootPageStack.push(Qt.resolvedUrl("../MessageView/OneMessagePage.qml"),
                               {
                                   mailboxName: oneMessagePage.mailboxName,
                                   uid: oneMessagePage.uid,
                                   partId: partId,
                                   editable: false
                               });
        }
        onComposeMessageTo: {
            rootPageStack.push(Qt.resolvedUrl("../Composer/ComposePage.qml"),
                               {
                                   replyMode: ReplyMode.REPLY_NONE,
                                   pageTitle:qsTr("New Message"),
                                   mailtoUrl: "mailto:" + address,
                                   isBottomEdgeMode: false

                               });
        }
        onAddToAddressbook: {
            addressbookModel.savePrettyEmail(address)
        }
    }

    Rectangle {
        id: btmshadow
        anchors {
            left: parent.left
            right: parent.right
            top: dekko.viewState.isSmallFF ? header.bottom : envelopeHeader.bottom
        }
        opacity: htmlView.contentY > 10 ? 0.8 : 0.0
        Behavior on opacity { UbuntuNumberAnimation{} }
        height: units.gu(0.8)
        gradient: Gradient {
            GradientStop { position: 0.0; color: UbuntuColors.warmGrey }
            GradientStop { position: 1.0; color: "transparent" }
        }
    }

    Metric {
        id: clicksMetric
        name: "email-read-count"
        format: "<b>%1</b> " + qsTr("e-mails read today")
        emptyFormat: qsTr("No e-mails read today")
        domain: "dekko.dekkoproject"
    }

    Timer {
        id: webviewTimer
        repeat: false
        interval: 1000
        onTriggered: htmlView.isScrolling = false
    }

    Timer {
        id: markAsReadTimer
        repeat: false
        interval: 50
        onTriggered: {
            dekko.currentAccount.msgModel.markMessageRead(true);
            if (dekko.currentAccount.isGmail) {
                console.log("Starting gmail hack to double mark \Seen")
                gmailHack.start()
            }
            clicksMetric.increment()
        }
    }

    Timer {
        id: gmailHack
        repeat: false
        interval: 500
        onTriggered: dekko.currentAccount.msgModel.markMessageRead(true);
    }

    Action {
        id: deleteAction
        iconName: "delete"
        text: qsTr("Delete")
        enabled: !htmlView.isScrolling && editable
        visible: dekko.currentAccount.imapModel.isNetworkOnline
        onTriggered: {
            dekko.currentAccount.msgModel.markMessageDeleted()
            if (GlobalSettings.preferences.autoExpungeOnDelete) {
                dekko.currentAccount.imapModel.expungeMailbox(dekko.currentAccount.msgListModel.currentMailbox())
            }
            backAction.trigger()
        }
    }

    Action {
        id: nextMsgAction
        iconName: "down"
        text: qsTr("Next message")
        enabled: uidIterator.nextUid > 0 && editable
        onTriggered: {
            messageBuilder.allowLoading = true
            uid = uidIterator.goNext();
            if (uid > 0) {
                dekko.currentAccount.msgModel.setMessage(dekko.currentAccount.msgListModel.mailboxName, uid)
            }
        }
    }

    Action {
        id: prevMsgAction
        iconName: "up"
        text: qsTr("Previous message")
        enabled: uidIterator.prevUid > 0 && editable
        onTriggered: {
            messageBuilder.allowLoading = true
            uid = uidIterator.goPrev();
            if (uid > 0) {
                dekko.currentAccount.msgModel.setMessage(dekko.currentAccount.msgListModel.mailboxName, uid)
            }
        }
    }

    RadialAction {
        id: replyAll
        iconSource: "qrc:///actions/mail-reply-all.svg"
        iconColor: UbuntuColors.coolGrey
        text: qsTr("Reply all")
        onTriggered: {
            rootPageStack.push(Qt.resolvedUrl("../Composer/ComposePage.qml"),
                               {
                                   composerReplyMode: true,
                                   pageTitle: fromName.length > 25 ? (fromName.slice(0,25) + "...") : fromName,
                                                                     replyMode: ReplyMode.REPLY_ALL,
                                                                     replyUid: oneMessagePage.uid,
                                                                     replyMboxName: oneMessagePage.mailboxName,
                                                                     replyPartId: partId,
                                                                     isBottomEdgeMode: false
                               });
        }
    }
    RadialAction {
        id: reply
        iconSource: "qrc:///actions/mail-reply.svg"
        iconColor: UbuntuColors.coolGrey
        top: true
        text: qsTr("Reply")
        onTriggered: {
            rootPageStack.push(Qt.resolvedUrl("../Composer/ComposePage.qml"),
                               {
                                   composerReplyMode: true,
                                   pageTitle: fromName.length > 25 ? (fromName.slice(0,25) + "...") : fromName,
                                                                     replyMode: ReplyMode.REPLY_PRIVATE,
                                                                     replyUid: oneMessagePage.uid,
                                                                     isBottomEdgeMode: false,
                                                                     replyMboxName: oneMessagePage.mailboxName,
                                                                     replyPartId: partId
                               });
        }
    }

    RadialAction {
        id: replyList
        iconSource: "qrc:///actions/mail-reply-all.svg"
        iconColor: UbuntuColors.coolGrey
        text: qsTr("Reply List")
        onTriggered: {
            rootPageStack.push(Qt.resolvedUrl("../Composer/ComposePage.qml"),
                               {
                                   composerReplyMode: true,
                                   pageTitle: fromName.length > 25 ? (fromName.slice(0,25) + "...") : fromName,
                                                                     replyMode: ReplyMode.REPLY_LIST,
                                                                     replyUid: oneMessagePage.uid,
                                                                     replyMboxName: oneMessagePage.mailboxName,
                                                                     replyPartId: partId,
                                                                     isBottomEdgeMode: false
                               });
        }
    }

    RadialAction {
        id: forward
        iconSource: "qrc:///actions/mail-forward.svg"
        iconColor: UbuntuColors.coolGrey
        text: qsTr("Forward")
        onTriggered: {
            rootPageStack.push(Qt.resolvedUrl("../Composer/ComposePage.qml"),
                               {
                                   composerReplyMode: true,
                                   pageTitle: qsTr("Forward"),
                                   replyMode: ReplyMode.REPLY_FORWARD,
                                   replyUid: oneMessagePage.uid,
                                   replyMboxName: oneMessagePage.mailboxName,
                                   replyPartId: partId,
                                   isBottomEdgeMode: false
                               });
        }
    }

    RadialBottomEdge {
        visible: dekko.currentAccount.imapModel.isNetworkOnline && dekko.viewState.isSmallFF
        actions: messageModel.isListPost ? [reply, replyAll, replyList, forward] : [reply, replyAll, forward]
    }


    Component {
        id: attachmentDialog
        Dialog {
            id: attachmentDownloadProgressDialog

            property alias progress: attachmentDownloadProgressbar.value

            title: qsTr("Downloading")
            contents: [
                ProgressBar {
                    id: attachmentDownloadProgressbar
                    maximumValue: 1
                    minimumValue: 0
                }
            ]
        }
    }

    AttachmentDownloader {
        id: attachmentDownloader
        messageModel: dekko.currentAccount.msgModel
        msgPartNAM: msgPartNAM

        property var progressDialog;
        property string fileUrl;

        onDownloadProgress: {
            if (!progressDialog) {
                progressDialog = PopupUtils.open(attachmentDialog);
            }
            progressDialog.progress = downloaded / total;
        }
        onDownloadComplete: {
            if (progressDialog) {
                progressDialog.progress = 1;
                openContentPickerTimer.start();
                fileUrl = url;
            } else {
                openContentPicker(url)
            }
        }
        onDownloadFailed: {
            console.log("Download Failed");
            if (progressDialog) {
                PopupUtils.close(progressDialog);
                progressDialog = false;
            }
        }

        function openContentPicker(url) {
            if (!url) {
                url = fileUrl;
            }
            var contentDialog = PopupUtils.open(Qt.resolvedUrl("../Dialogs/ContentPickerDialog.qml"),
                                                oneMessagePage, {
                                                    fileUrl: url
                                                });
            contentDialog.downloadFilePath = url;
            contentDialog.complete.connect(function() {
                attachmentDownloader.cleanTmpFile();
            });
        }
    }

    Timer {
        id: openContentPickerTimer
        interval: 1000
        repeat: false
        onTriggered: {
            PopupUtils.close(attachmentDownloader.progressDialog);
            attachmentDownloader.progressDialog = false;
            attachmentDownloader.openContentPicker();
        }
    }

    MsgPartNetAccessManager {
        id: msgPartNAM
    }

    UidIterator {
        id: uidIterator
        messageListModel: oneMessagePage.messageListModel ? oneMessagePage.messageListModel : null
        uid: dekko.currentAccount.msgModel.uid
    }
}
