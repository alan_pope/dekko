/* Copyright (C) 2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.3
import Ubuntu.Components 1.1

Icon {
    id: root

    property bool starred

    signal clicked;

    name: starred ? "starred" : "favorite-unselected"
    width: units.gu(2)
    height: width
    anchors {
        right: parent.right
        rightMargin: units.gu(1)
        verticalCenter: parent.verticalCenter
    }
    MouseArea {
        id: mouseArea
        anchors.fill: parent
        anchors.margins: units.gu(-1)
        onClicked: root.clicked();
    }
    Rectangle {
        anchors.fill: parent
        anchors.margins: units.gu(-1)
        opacity: 0.1
        color: UbuntuColors.coolGrey
        visible: mouseArea.pressed
    }
}

