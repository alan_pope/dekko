/* Copyright (C) 2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.3
import Ubuntu.Components 1.1
import DekkoCore 0.2
import "../Components"
import "imports"

ListItemWithActions {
    id: root

    property bool displayTime: false
    property bool displayStar: false

    property bool isStarred: starred

    signal removed

    width: parent.width
    leftSideAction: Action {
        iconName: "delete"
        onTriggered: root.removed();
    }

    function formatHourMinute(date) {
        if (!date)
            return "";
        var hour = date.getHours();
        if (hour < 10)
            hour = "0" + hour;
        var minute = date.getMinutes();
        if (minute < 10)
            minute = "0" + minute;
        return hour + ":" + minute;
    }

    ContactAvatar {
        id: avatar
        anchors {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
        }
        width: height
    }
    Column {
        anchors {
            left: avatar.right
            right: parent.right
            margins: units.gu(1)
            verticalCenter: parent.verticalCenter
        }

        Label {
            id: name
            objectName: "nameLabel"
            color: Style.common.text
            width: parent.width
            text: (firstName + " " + lastName).trim()
            elide: Text.ElideRight
            fontSize: "medium"
            font.bold: true
            height: text ? implicitHeight : 0
        }

        Label {
            width: parent.width
            text: emails.length > 1 ? (emails.length + " " + qsTr("emails")): emails[0]
            color: Style.common.text
            elide: Text.ElideRight
            fontSize: name.height > 0 ? "small" : "medium"
        }
    }

    StarButton {
        starred: root.isStarred
        visible: displayStar
        anchors {
            right: parent.right
            rightMargin: units.gu(1)
            verticalCenter: parent.verticalCenter
        }
        onClicked: isStarred = !isStarred
    }

    Label {
        visible: displayTime
        text: formatHourMinute(lastContactDate)
        fontSize: "small"
        anchors {
            right: parent.right
            top: parent.top
            margins: units.gu(1)
        }
    }
}
