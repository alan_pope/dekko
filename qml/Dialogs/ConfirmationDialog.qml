import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.Popups 1.0

Dialog {
    id: confirmationDialog

    property Action confirmAction
    property Action cancelAction
    property alias confirmButtonText: confirmButton.text
    property alias cancelButtonText: cancelButton.text
    property alias confirmButton: confirmButton
    property alias cancelButton: cancelButton

    signal confirmClicked
    signal cancelClicked

    contents: [

        Row {
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: units.gu(1)

            Button {
                id: cancelButton
                text: qsTr("Cancel")
                color: UbuntuColors.red
                width: parent.width / 2 - units.gu(0.5)
                onClicked: {
                    if (cancelAction) {
                        cancelAction.triggered(cancelButton)
                    }
                    PopupUtils.close(confirmationDialog)
                    cancelClicked()
                }
            }
            Button {
                id: confirmButton
                text: qsTr("Confirm")
                color: UbuntuColors.green
                width: parent.width / 2 - units.gu(0.5)
                onClicked: {
                    if (confirmAction) {
                        confirmAction.triggered(confirmButton)
                    }
                    PopupUtils.close(confirmationDialog)
                    confirmClicked()
                }
            }
        }
    ]
}
