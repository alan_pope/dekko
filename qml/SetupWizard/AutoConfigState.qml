/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.4
import QtQml.StateMachine 1.0 as DSM
import DekkoCore 0.2

DSM.State {
    id: autoConfig
    signal success()
    signal hasSmtp()
    signal fail()
    property var overlay: undefined

    property alias failedTargetState: failTransition.targetState
    property alias successTargetState: successTransition.targetState
    property alias smtpConfigFoundTargetState: smtpConfigFoundTransition.targetState

    function handleMailConfig(mailConfig) {

        commitMailConfig(mailConfig)
        // Use a switch here to ensure we only
        // continue for *known* account types
        switch(accountType) {
        case NewAccountType.IMAP:
            if (account.smtpSettings.server) {
                hasSmtp()
            } else {
                success()
            }
            break
        case NewAccountType.SMTP:
            success()
            break
        }
    }

    function commitMailConfig(mailConfig) {
        switch (accountType) {
        case NewAccountType.IMAP:
            account.imapSettings.server = mailConfig.imapHost
            account.imapSettings.port = mailConfig.imapPort
            account.imapSettings.connectionMethod = mailConfig.imapMethod
            account.imapSettings.authenticationMethod = ImapSettings.LOGIN
            account.smtpSettings.server = mailConfig.smtpHost
            account.smtpSettings.port = mailConfig.smtpPort
            account.smtpSettings.submissionMethod = mailConfig.smtpMethod
            account.smtpSettings.authenticationMethod = SmtpSettings.LOGIN
            break;
        case NewAccountType.SMTP:
            account.smtpSettings.server = mailConfig.smtpHost
            account.smtpSettings.port = mailConfig.smtpPort
            account.smtpSettings.submissionMethod = mailConfig.smtpMethod
            account.smtpSettings.authenticationMethod = SmtpSettings.LOGIN
            break;
        }
    }

    onEntered: {
        var c = Qt.createComponent(Qt.resolvedUrl("AutoConfigOverlay.qml"))
        overlay = c.createObject(wizard)
        overlay.autoconfig.failed.connect(fail)
        overlay.autoconfig.success.connect(handleMailConfig)
    }
    onExited: overlay.destroy()

    DSM.SignalTransition {
        id: successTransition
        signal: autoConfig.success
    }

    DSM.SignalTransition {
        id: failTransition
        signal: autoConfig.fail
    }

    DSM.SignalTransition {
        id: smtpConfigFoundTransition
        signal: autoConfig.hasSmtp
    }

}
