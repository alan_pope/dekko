/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.3
import Ubuntu.Components 1.2
import DekkoCore 0.2
import "../Components"

Item {
    id: serverInput
    anchors.fill: parent

    signal back()
    signal inputComplete()

    property ImapSettings imapSettings: wizard.account.imapSettings
    property SmtpSettings smtpSettings: wizard.account.smtpSettings
    readonly property bool imapSettingsValid: imapSettings.username && imapSettings.server && imapSettings.port
    readonly property bool smtpSettingsValid: smtpSettings.username && smtpSettings.server && smtpSettings.port


    DekkoHeader {
        id: header
        title: qsTr("Server details")
        width: parent.width
        height: units.gu(7)
        backAction: bkAction
        primaryAction: nextAction
    }

    Action {
        id: bkAction
        iconName: "back"
        onTriggered: back()
    }

    Action {
        id: nextAction
        iconName: "next"
        onTriggered: {
            switch (wizard.accountType) {
            case NewAccountType.IMAP:
                internal.saveImap()
                if (!imapSettingsValid) {
                    internal.invalidFields = true
                    return
                }
                break
            case NewAccountType.SMTP:
                internal.saveSmtp()
                if (!smtpSettingsValid) {
                    internal.invalidFields = true
                    return
                }
                break
            case NewAccountType.PRESET:
            case NewAccountType.IMAP_WITH_ADDED_SMTP:
                internal.saveImap()
                internal.saveSmtp()
                if (!imapSettingsValid || !smtpSettingsValid) {
                    internal.invalidFields = true
                    return
                }
                break
            }
            inputComplete()
        }
    }

    QtObject {
        id: internal
        property bool invalidFields: false

        function saveImap() {
            wizard.account.imapSettings.username = manualSetup.imapUsername
            wizard.account.imapSettings.server = manualSetup.imapServer
            wizard.account.imapSettings.port = manualSetup.parsedImapPort()
            console.log("ENCRYPTION METHOD: ", manualSetup.imapEncryptionMethod)
            wizard.account.imapSettings.connectionMethod = manualSetup.imapEncryptionMethod
            wizard.tempPasswordStore = manualSetup.imapPassword
        }

        function saveSmtp() {
            smtpSettings.username = manualSetup.smtpUsername
            smtpSettings.server = manualSetup.smtpServer
            smtpSettings.port = manualSetup.parsedSmtpPort()
            smtpSettings.submissionMethod = manualSetup.smtpEncryptionMethod
            wizard.tempSmtpPasswordStore = manualSetup.smtpPassword
        }
    }

    Component.onCompleted: {
        switch (wizard.accountType) {
        case NewAccountType.IMAP:
            state = "imap"
            break
        case NewAccountType.SMTP:
            state = "smtp"
            break
        case NewAccountType.PRESET:
        case NewAccountType.IMAP_WITH_ADDED_SMTP:
            state = "imap-smtp"
            break
        }
    }

    states: [
        State {
            name: "imap"
            PropertyChanges {
                target: manualSetup
                imapEncryptionMethod: imapSettings.connectionMethod
                imapServer: imapSettings.server
                imapPort: imapSettings.port
                imapUsername: imapSettings.username
                imapPassword: wizard.tempPasswordStore
            }
        },
        State {
            name: "smtp"
            PropertyChanges {
                target: manualSetup
                smtpEncryptionMethod: smtpSettings.submissionMethod
                smtpServer: smtpSettings.server
                smtpPort: smtpSettings.port
                smtpUsername: smtpSettings.username
                smtpPassword: wizard.tempSmtpPasswordStore
            }
        },
        State {
            name: "imap-smtp"
            PropertyChanges {
                target: manualSetup
                imapEncryptionMethod: imapSettings.connectionMethod
                imapServer: imapSettings.server
                imapPort: imapSettings.port
                imapUsername: imapSettings.username
                imapPassword: wizard.tempPasswordStore
                smtpEncryptionMethod: smtpSettings.submissionMethod
                smtpServer: smtpSettings.server
                smtpPort: smtpSettings.port
                smtpUsername: smtpSettings.username
                smtpPassword: wizard.tempSmtpPasswordStore
            }
        }
    ]

    Flickable {
        anchors {
            left: parent.left
            top: header.bottom
            right: parent.right
            bottom: parent.bottom
        }
        contentHeight: manualSetup.height + units.gu(5)
        ManualSetup {
            id: manualSetup
            spacing: units.gu(1)

            onImapPortChanged: {
                console.log("PORT CHANGED: ", parsedImapPort())
            }

            anchors {
                left: parent.left
                top: parent.top
                topMargin: units.gu(2)
                right: parent.right
            }
        }
    }
}
