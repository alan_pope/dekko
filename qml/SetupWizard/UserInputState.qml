/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.4
import QtQml.StateMachine 1.0 as DSM
import DekkoCore 0.2

DSM.State {
    id: userInputState

    signal hasDomain()
    signal noDomain()
    signal back()

    property alias backTargetState: backTransition.targetState
    property alias hasDomainTargetState: hasDomainTransition.targetState
    property alias noDomainTargetState: noDomainTransition.targetState

    function determineConnectionMethods(conType) {
        if (conType === "imap") {
            if (selectedPreset.imapSSL) {
                return ImapSettings.SSL
            } else if (selectedPreset.imapStartTls) {
                return ImapSettings.STARTTLS
            } else {
                return ImapSettings.NONE
            }
        } else if (conType === "smtp") {
            if (selectedPreset.smtpSSL) {
                return SmtpSettings.SSMTP;
            } else if (selectedPreset.smtpStartTls) {
                return SmtpSettings.SMTP_STARTTLS;
            } else {
                return SmtpSettings.SMTP;
            }
        } else {
            console.log("Cannot determine connection for unknown type: " + conType);
        }
    }


    initialState: userInput

    DSM.State {
        id: userInput
        signal inputComplete()
        signal cleanup()
        onEntered: {
            currentComponent = Qt.createComponent(Qt.resolvedUrl("UserInput.qml"))
            loader.item.inputComplete.connect(userInput.inputComplete)
            loader.item.back.connect(cleanup)
        }
        onExited: {
            loader.item.inputComplete.disconnect(userInput.inputComplete)
            loader.item.back.disconnect(cleanup)
        }

        DSM.SignalTransition {
            targetState: commitDetails
            signal: userInput.inputComplete
        }

        DSM.SignalTransition {
            targetState: cleanupDetails
            signal: userInput.cleanup
        }

    }

    DSM.State {
        id: cleanupDetails
        onEntered: {
            // unwind the stored settings
            account.imapSettings.username = ""
            account.smtpSettings.username = ""
            tempPasswordStore = ""
            tempSmtpPasswordStore = ""
            identity.name = ""
            identity.email = ""
            identity.organization = ""
            userInputState.back()
        }
    }

    DSM.State {
        id: commitDetails

        onEntered: {
            // here we commit preset config & check username is already commited
            // but we do not call save() until the wizard is complete.
            if (accountType === NewAccountType.PRESET) {
                account.imapSettings.server = selectedPreset.imapServer
                account.imapSettings.port = selectedPreset.imapPort
                account.imapSettings.connectionMethod = determineConnectionMethods("imap")
                account.imapSettings.authenticationMethod = ImapSettings.LOGIN
                account.smtpSettings.server = selectedPreset.smtpServer
                account.smtpSettings.port = selectedPreset.smtpPort
                account.smtpSettings.submissionMethod = determineConnectionMethods("smtp")
                account.smtpSettings.authenticationMethod = SmtpSettings.LOGIN
            }
            if (EmailValidator.validate(account.imapSettings.username)) {
                userInputState.hasDomain()
            } else {
                userInputState.noDomain()
            }
        }
    }

    DSM.SignalTransition {
        id: hasDomainTransition
        targetState: wizard.accountType === NewAccountType.PRESET ? validateCredentials : autoConfig
        signal: userInputState.hasDomain
    }

    DSM.SignalTransition {
        id: noDomainTransition
        signal: userInputState.noDomain
    }

    DSM.SignalTransition {
        id: backTransition
        signal: userInputState.back
    }
}
