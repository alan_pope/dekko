/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0
import DekkoCore 0.2
import "../Components"

Column {
    id: column

    anchors {
        left: parent.left
        top: parent.top
        right: parent.right
    }

    property alias imapEncryptionMethod: imapEncryptionSelector.method
    property alias imapServer: imapServerField.text
    property string imapPort: imapPortField.text
    function parsedImapPort(){ return parseInt(imapPortField.text)}
    property alias imapUsername: imapEmailField.text
    property alias imapPassword: imapPasswordField.text
    property alias smtpEncryptionMethod: smtpEncryptionSelector.method
    property alias smtpServer: smtpServerField.text
    property alias smtpPort: smtpPortField.text
    function parsedSmtpPort(){ return parseInt(smtpPortField.text)}
    property alias smtpUsername: smtpEmailField.text
    property alias smtpPassword: smtpPasswordField.text
    readonly property bool isImap: serverInput.state === "imap"
    readonly property bool isSmtp: serverInput.state === "smtp"

    onVisibleChanged: {
        if (visible) {
            nameField.forceActiveFocus()
            nameField.textFieldFocusHandle.focus = true
        }
    }

    SectionDivider {
        text: qsTr("Incoming Mail Server")
        visible: !isSmtp
        showDivider: false
    }

    Item {
        width: parent.width
        height: col1.height
        visible: !isSmtp
        Column {
            id: col1
            anchors {
                left: parent.left
                top: parent.top
                right: parent.right
            }
            Rectangle {
                width: parent.width
                height: units.dp(2)
                gradient: Gradient {
                    GradientStop { position: 0.0; color: Qt.rgba(0, 0, 0, 0.1) }
                    GradientStop { position: 0.49; color: Qt.rgba(0, 0, 0, 0.1) }
                    GradientStop { position: 0.5; color: Qt.rgba(1, 1, 1, 0.4) }
                    GradientStop { position: 1.0; color: Qt.rgba(1, 1, 1, 0.4) }
                }
            }
            EncryptionSelector {
                id: imapEncryptionSelector
                type: "imap"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                onPortChanged: imapPortField.text = port
            }
        }
    }

    TitledTextField {
        id: imapServerField
        title: qsTr("Host name")
        visible: !isSmtp
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
        sendTabEventOnEnter: true
        requiredField: internal.invalidFields
        placeholderText: qsTr("Eg: \"imap.example.com\"")
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: imapPortField.textFieldFocusHandle
    }

    TitledTextField {
        id: imapPortField
        title: qsTr("Port")
        visible: !isSmtp
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText | Qt.ImhDigitsOnly
        sendTabEventOnEnter: true
        requiredField: internal.invalidFields
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: imapEmailField.textFieldFocusHandle
    }

    TitledTextField {
        id: imapEmailField
        title: qsTr("Username")
        visible: !isSmtp
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhEmailCharactersOnly | Qt.ImhNoPredictiveText
        sendTabEventOnEnter: true
        requiredField: internal.invalidFields
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: imapPasswordField.textFieldFocusHandle
    }

    TitledTextField {
        id: imapPasswordField
        title: qsTr("Password")
        visible: !isSmtp
        sendTabEventOnEnter: true
        inputMethodHints: Qt.ImhHiddenText | Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
        echoMode: showImapPassword.checked ? TextInput.Normal : TextInput.Password
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: smtpServerField.textFieldFocusHandle
    }

    CheckboxWithLabel {
        id: showImapPassword
        text: qsTr("Show password")
        visible: !isSmtp
        anchors {
            left:parent.left
            right: parent.right
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }
    }

    SectionDivider {
        text: qsTr("Outgoing Mail Server")
        visible: !isImap
        showDivider: false
        anchors {}
    }

    Item {
        width: parent.width
        height: col.height
        visible: !isImap
        Column {
            id: col
            anchors {
                left: parent.left
                top: parent.top
                right: parent.right
            }
            Rectangle {
                width: parent.width
                height: units.dp(2)
                gradient: Gradient {
                    GradientStop { position: 0.0; color: Qt.rgba(0, 0, 0, 0.1) }
                    GradientStop { position: 0.49; color: Qt.rgba(0, 0, 0, 0.1) }
                    GradientStop { position: 0.5; color: Qt.rgba(1, 1, 1, 0.4) }
                    GradientStop { position: 1.0; color: Qt.rgba(1, 1, 1, 0.4) }
                }
            }
            EncryptionSelector {
                id: smtpEncryptionSelector
                type: "smtp"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                onPortChanged: smtpPortField.text = port
            }
        }
    }

    TitledTextField {
        id: smtpServerField
        title: qsTr("Host name")
        visible: !isImap
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
        sendTabEventOnEnter: true
        requiredField: internal.invalidFields
        placeholderText: qsTr("Eg \"smtp.example.com\"")
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: smtpPortField.textFieldFocusHandle
    }

    TitledTextField {
        id: smtpPortField
        title: qsTr("Port")
        visible: !isImap
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText | Qt.ImhDigitsOnly
        sendTabEventOnEnter: true
        requiredField: internal.invalidFields
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: smtpEmailField.textFieldFocusHandle
    }

    TitledTextField {
        id: smtpEmailField
        title: qsTr("Username")
        visible: !isImap
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhEmailCharactersOnly | Qt.ImhNoPredictiveText
        sendTabEventOnEnter: true
        requiredField: internal.invalidFields
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: smtpPasswordField.textFieldFocusHandle
    }

    TitledTextField {
        id: smtpPasswordField
        title: qsTr("Password")
        visible: !isImap
        sendTabEventOnEnter: true
        inputMethodHints: Qt.ImhHiddenText | Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
        echoMode: showSmtpPassword.checked ? TextInput.Normal : TextInput.Password
        KeyNavigation.priority: KeyNavigation.BeforeItem
    }

    CheckboxWithLabel {
        id: showSmtpPassword
        text: qsTr("Show password")
        visible: !isImap
        anchors {
            left:parent.left
            right: parent.right
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }
    }
}
