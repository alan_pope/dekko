/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.3
import Ubuntu.Components 1.2
import Ubuntu.Components.Popups 1.0
import DekkoCore 0.2
import "../Components"

Item {
    id: userInput
    anchors.fill: parent

    signal back()
    signal inputComplete()

    DekkoHeader {
        id: header
        title: qsTr("Account description")
        width: parent.width
        height: units.gu(7)
        backAction: bkAction
        primaryAction: nextAction
    }

    Action {
        id: bkAction
        iconName: "back"
        onTriggered: {
            back()
        }
    }

    Action {
        id: nextAction
        iconName: "next"
        onTriggered: {
            if (descriptionField.text) {
                wizard.account.profile.description = descriptionField.text
            } else {
                switch (wizard.accountType) {
                case NewAccountType.IMAP:
                case NewAccountType.IMAP_WITH_ADDED_SMTP:
                case NewAccountType.ONLINE_ACCOUNT:
                    wizard.account.profile.description = wizard.account.imapSettings.username
                    break
                case NewAccountType.SMTP:
                    wizard.account.profile.description = wizard.account.smtpSettings.username
                    break
                }
            }
            inputComplete()
        }
    }

    Column {
        anchors {
            left: parent.left
            top: header.bottom
            topMargin: units.gu(2)
            right: parent.right
        }

        TitledTextField {
            id: descriptionField
            title: qsTr("Description (optional)")
            placeholderText: qsTr("Eg: \"Personal\" or \"Work\"")
            Component.onCompleted: {
                forceActiveFocus()
                textFieldFocusHandle.focus = true
            }
        }
    }
}
