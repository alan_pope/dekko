/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.4
import Ubuntu.Components 1.2

Item {
    anchors.fill: parent
    signal success()
    signal failed()
    property alias text: label.text

    Rectangle {
        color: "black"
        opacity: 0.9
        anchors.fill: parent
    }
    Label {
        id: label
        anchors {
            bottom: activity.top
            bottomMargin: units.gu(3)
            horizontalCenter: parent.horizontalCenter
        }
        //        width: parent.width - units.gu(4)
        fontSize: "large"
        color: "white"
    }

    ActivityIndicator {
        id: activity
        running: true

        anchors.centerIn: parent

    }
}
