/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.4
import DekkoCore 0.2
import Ubuntu.Components 1.2
import Ubuntu.Components.Popups 1.0
import Ubuntu.OnlineAccounts 0.1 as OA
import Ubuntu.OnlineAccounts.Client 0.1 as OAC

Item {
    id: newOA

    signal successAquiringUoaId()
    property int uoaId: 0

    OAC.Setup {
        id: setup
        applicationId: "dekko.dekkoproject_dekko"
        providerId: "google"

        Component.onCompleted: exec()
    }

    OnlineAccountsService {
        onNewAccountEnabled: {
            wizard.oaId = id;
            successAquiringUoaId()
        }
    }

}
