/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.3
import Ubuntu.Components 1.2
import DekkoCore 0.2
import "../Components"

Item {
    id: userInput
    anchors.fill: parent

    signal back()
    signal inputComplete()

    DekkoHeader {
        id: header
        width: parent.width
        height: units.gu(7)
        backAction: bkAction
        primaryAction: nextAction
    }

    Action {
        id: bkAction
        iconName: "back"
        onTriggered: back()
    }

    Action {
        id: nextAction
        iconName: "next"
        onTriggered: {
            if (!emailField.text) {
                nonValidUsernameField = true
                return
            }

            if (state === "pre-set") {
                if (!EmailValidator.validate(emailField.text)) {
                    nonValidUsernameField = true
                    return
                }
            }
            wizard.account.imapSettings.username = emailField.text
            wizard.account.smtpSettings.username = emailField.text
            if (passwordField.text) {
                switch(wizard.accountType) {
                case NewAccountType.PRESET:
                    wizard.tempPasswordStore = passwordField.text
                    wizard.tempSmtpPasswordStore = passwordField.text
                    break
                case NewAccountType.IMAP:
                    wizard.tempPasswordStore = passwordField.text
                    wizard.tempSmtpPasswordStore = passwordField.text
                    break;
                case NewAccountType.SMTP:
                    wizard.tempSmtpPasswordStore = passwordField.text
                    break;
                }
            }
            inputComplete()
        }
    }

    Component.onCompleted: {
        switch(wizard.accountType) {
        case NewAccountType.PRESET:
            console.log("PRESET")
            state = "pre-set"
            break;
        case NewAccountType.IMAP:
            console.log("IMAP")
            state = "imap"
            break;
        case NewAccountType.SMTP:
            console.log("SMTP")
            state = "smtp"
            break;
        }
        emailField.forceActiveFocus()
        emailField.textFieldFocusHandle.focus = true
    }

    states: [
        State {
            name: "pre-set"
            PropertyChanges {
                target: emailField
                title: qsTr("Email address")
            }
            PropertyChanges {
                target: header
                title: wizard.selectedPreset.description
            }
        },
        State {
            name: "imap"
            PropertyChanges {
                target: emailField
                title: qsTr("Username")
            }
            PropertyChanges {
                target: header
                title: qsTr("IMAP account")
            }
        },
        State {
            name: "smtp"
            PropertyChanges {
                target: emailField
                title: qsTr("Username")
            }
            PropertyChanges {
                target: header
                title: qsTr("SMTP account")
            }
        }
    ]

    property bool nonValidUsernameField: false
    Flickable {
        id: flicker
        anchors {
            left: parent.left; right: parent.right
            top: header.bottom; bottom: parent.bottom
        }

        contentHeight: col.height
        Column {
            id: col
            anchors {
                left: parent.left
                top: parent.top
                topMargin: units.gu(2)
                right: parent.right
            }
            spacing: units.gu(1)
            TitledTextField {
                id: emailField
                text: wizard.accountType === NewAccountType.SMTP ? wizard.account.smtpSettings.username : wizard.account.imapSettings.username
                requiredField: nonValidUsernameField
                inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhEmailCharactersOnly | Qt.ImhNoPredictiveText
                sendTabEventOnEnter: true
                placeholderText: qsTr("Eg: \"johnsmith@example.com\"")
                KeyNavigation.priority: KeyNavigation.BeforeItem
                KeyNavigation.tab: passwordField.textFieldFocusHandle
            }
            Label {
                anchors.left: parent.left
                anchors.leftMargin: units.gu(2.5)
                anchors.right: parent.right
                anchors.rightMargin: units.gu(2.5)
                color: "red"
                wrapMode: Text.Wrap
                visible: nonValidUsernameField
                text: qsTr("A valid email address is required for this service provider")
            }

            TitledTextField {
                id: passwordField
                title: qsTr("Password")
                text: wizard.tempPasswordStore
                inputMethodHints: Qt.ImhHiddenText | Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
                echoMode: showPassword.checked ? TextInput.Normal : TextInput.Password
                KeyNavigation.priority: KeyNavigation.BeforeItem
            }
            CheckboxWithLabel {
                id: showPassword
                anchors {
                    left:parent.left
                    right: parent.right
                    leftMargin: units.gu(2)
                    rightMargin: units.gu(2)
                }
                text: qsTr("Show password")
            }
        }
    }
}
