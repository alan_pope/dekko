/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.4
import QtQml.StateMachine 1.0 as DSM

DSM.State {
    id: noAccounts
    signal createAccount()
    signal quit()
    property alias backTargetState: finishedTransition.targetState
    property alias nextTargetState: createTransition.targetState
    onEntered: {
        currentComponent = Qt.createComponent(Qt.resolvedUrl("NoAccountsPage.qml"))
        loader.item.createAccount.connect(createAccount)
        loader.item.quit.connect(quit)
    }
    onExited: {
        loader.item.createAccount.disconnect(createAccount)
        loader.item.quit.disconnect(quit)
    }
    DSM.SignalTransition {
        id: createTransition
        signal: noAccounts.createAccount
    }
    DSM.SignalTransition {
        id: finishedTransition
        signal: noAccounts.quit
    }
}
