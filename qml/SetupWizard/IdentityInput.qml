/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.3
import Ubuntu.Components 1.2
import Ubuntu.Components.Popups 1.0
import DekkoCore 0.2
import "../Components"
import "../Settings/user"

Item {
    id: userInput
    anchors.fill: parent

    signal back()
    signal inputComplete()

    property alias name: input.nameField
    property alias email: input.emailField
    property alias organization: input.organizationField

    DekkoHeader {
        id: header
        title: qsTr("Create identity")
        width: parent.width
        height: units.gu(7)
        backAction: bkAction
        primaryAction: nextAction
    }

    Action {
        id: bkAction
        iconName: "back"
        onTriggered: {
            back()
        }
    }

    Action {
        id: nextAction
        iconName: "next"
        onTriggered: {
            if (email === "" || name === "") {
                input.showMissingFields = true
                return
            }
            if (!EmailValidator.validate(email)) {
                PopupUtils.open(Qt.resolvedUrl("../Dialogs/InfoDialog.qml"), wizard, {title: qsTr("Invalid address"), text: qsTr("%1 is not a valid email address").arg(email)})
                return
            }
            inputComplete()
        }
    }

    Flickable {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        contentHeight: input.height + units.gu(5)
        SenderIdentityInput {
            id: input
            signatureFieldVisible: false
            nameField: wizard.identity.name
            emailField: EmailValidator.validate(wizard.account.smtpSettings.username) ? wizard.account.smtpSettings.username : wizard.identity.email
            organizationField: wizard.identity.organization
        }
    }
}
