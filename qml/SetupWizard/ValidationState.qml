/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.4
import QtQml.StateMachine 1.0 as DSM
import Ubuntu.Components.Popups 1.0
import DekkoCore 0.2

DSM.State {
    id: validateCredentials
    signal success()
    signal fail()
    property var overlay: undefined
    property var dlg: undefined
    property bool failed: false

    property alias failedTargetState: failTransition.targetState
    property alias successTargetState: successTransition.targetState

    function handleFailedWithErrorMessage(message) {
        failed = true;
        overlay.destroy()
        dlg = PopupUtils.open(Qt.resolvedUrl("../Dialogs/InfoDialog.qml"), wizard, {title: qsTr("Authentication failed"), text: message})
        dlg.closing.connect(fail)
    }

    onEntered: {
        failed = false
        var c = Qt.createComponent(Qt.resolvedUrl("CredentialValidation.qml"))
        overlay = c.createObject(wizard)
        overlay.success.connect(success)
        overlay.failedWithErrorMessage.connect(handleFailedWithErrorMessage)
    }

    onExited: {
        if (!failed) {
            overlay.destroy()
        }
    }

    DSM.SignalTransition {
        id: failTransition
        targetState: accountType === NewAccountType.PRESET ? userInputState : manualInput
        signal: validateCredentials.fail
    }

    DSM.SignalTransition {
        id: successTransition
        targetState: (accountType === NewAccountType.IMAP_WITH_ADDED_SMTP || accountType === NewAccountType.PRESET) ? identityInput : descriptionInput
        signal: validateCredentials.success
    }
}
