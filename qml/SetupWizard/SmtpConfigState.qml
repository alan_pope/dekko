/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.4
import QtQml.StateMachine 1.0 as DSM
import Ubuntu.Components.Popups 1.0
import DekkoCore 0.2

DSM.State {
    id: includeSmtpConfig
    signal proceed()
    property var dialog: undefined
    property alias succeedTargetState: proceedTransition.targetState

    property string msg: qsTr("SMTP configuration found for username: \n\n%1\n\nWould you like to configure this now?").arg(account.smtpSettings.username)

    function handleConfirmed() {
        accountType = NewAccountType.IMAP_WITH_ADDED_SMTP
        proceed()
    }

    function handleRejected() {
        account.smtpSettings.server = ""
        account.smtpSettings.port = 0
        account.smtpSettings.submissionMethod = SmtpSettings.SMTP
        account.smtpSettings.authenticationMethod = SmtpSettings.PLAIN
        account.smtpSettings.username = ""
        tempSmtpPasswordStore = ""
        proceed()
    }

    onEntered: {
        dialog = PopupUtils.open(Qt.resolvedUrl("../Dialogs/ConfirmationDialog.qml"), wizard, {
                                     confirmButtonText: qsTr("Yes"),
                                     cancelButtonText: qsTr("No"),
                                     title: qsTr("Settings found"),
                                     text: msg
                                 })
        dialog.confirmClicked.connect(handleConfirmed)
        dialog.cancelClicked.connect(handleRejected)
    }

    DSM.SignalTransition {
        id: proceedTransition
        targetState: validateCredentials
        signal: includeSmtpConfig.proceed
    }
}
