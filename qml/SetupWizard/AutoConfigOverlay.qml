/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.4
import DekkoCore 0.2

ProcessingOverlay {
    id: autoConfigOverlay

    property alias autoconfig: autoConfig

    state: "running"
    states: [
        State {
            name: "running"
            PropertyChanges {
                target: autoConfigOverlay
                text: qsTr("Looking for configuration...")
            }
        },
        State {
            name: "complete"
            PropertyChanges {
                target: autoConfigOverlay
                text: qsTr("Configuration found.")
            }
        },
        State {
            name: "failed"
            PropertyChanges {
                target: autoConfigOverlay
                text: qsTr("No configuration found.")
            }
        }
    ]

    readonly property bool isRunning: state === "running"

    AutoConfig {
        id: autoConfig
        emailAddress: wizard.account.imapSettings.username
        onSuccess: autoConfigOverlay.state = "complete"
        onFailed: autoConfigOverlay.state = "failed"
        Component.onCompleted: start()
    }

}
