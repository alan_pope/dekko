/* Copyright (C) 2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of the Dekko email client for Ubuntu Touch/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.2
import DekkoCore 0.2
import "../Components"

//Temporary page to display thread messages
DekkoPage {
    property int msgUid;
    pageTitle: ""
    accountsDrawerVisible: false

    signal messageSelected(int msgUid);

    ListView {
        anchors {
            fill: parent
            topMargin: units.gu(8)
        }
        model: dekko.currentAccount.singleThreadModel
        delegate: MessageListDelegate {
            anchors {
                left: parent.left
                right: parent.right
            }
            onItemClicked: {
                messageSelected(messageUid)
            }
        }
    }
}
