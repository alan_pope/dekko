import QtQuick 2.4
import Ubuntu.OnlineAccounts 0.1 as OA

Item {
    id: requestItem

    property string accountId
    property int uoaId

    signal accessToken(int accountId, string accessToken)

    OA.Account {
        id: account

        Component.onCompleted: {
            objectHandle = OA.Manager.loadAccount(uoaId)
        }
    }

    OA.AccountService {
        id: accountService

        Binding on objectHandle {
            value: account.accountServiceHandle
            when: account.accountId === uoaId
        }

        onObjectHandleChanged: {
            if (objectHandle && enabled) {
                authenticate()
            }
        }
        onAuthenticated: {
            accessToken(uoaId, reply.AccessToken)
            console.log("Access token is " + reply.AccessToken)
            requestItem.destroy()
        }
        onAuthenticationError: { console.log("Authentication failed, code " + error.code) }
    }
}
